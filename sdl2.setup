;;;; -*- Scheme -*-

(include "lib/version.scm")
(define version (apply sprintf "~A.~A.~A" (egg-version)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; The logic for determining compiler flags is pretty messy, to make
;;; the install process painless for most users.

(define (print-help-and-exit!)
  (print "
Error: could not automatically determine SDL2 flags.

Try setting the SDL2_CFLAGS and SDL2_LDFLAGS environment variables
to specify what compiler and linker flags to use. Example:

    export SDL2_CFLAGS=\"-I/usr/local/include/SDL2\"
    export SDL2_LDFLAGS=\"-L/usr/local/lib -lSDL2\"
    chicken-install sdl2")
  (exit 1))


(define (try-command command)
  (receive (out in pid err) (process* command)
    (receive (_ normal-exit? status) (process-wait pid)
      (and normal-exit?
           (zero? status)
           (string-trim
            (string-join (with-input-from-port out read-lines)
                         " "))))))

(define (has-mac-framework? name)
  (any (lambda (dir)
         (file-exists? (sprintf "~A/~A.framework" dir name)))
       '("/Library/Frameworks"
         "/System/Library/Frameworks")))

(define (token-prefixer prefix)
  (lambda (str)
    (map (cut string-append prefix <>)
         (string-split str))))


(define (get-flags #!key prefixer env-var command mac-framework)
  (prefixer
   (or
    ;; If the env var is set, it takes highest precedence.
    (get-environment-variable env-var)
    ;; Othewise, try running the command.
    (try-command command)
    ;; If that didn't work, see if the Mac framework is available.
    (cond ((not (eq? 'macosx (software-version)))
           #f)
          ((string-null? mac-framework)
           "")
          ((has-mac-framework? mac-framework)
           (string-append "-framework " mac-framework))
          (else #f))
    ;; Otherwise, abort.
    (print-help-and-exit!))))


(define sdl2-flags-list
  (append
   (get-flags prefixer: (token-prefixer "-C ")
              env-var: "SDL2_CFLAGS"
              command: "sdl2-config --cflags"
              mac-framework: "")
   (get-flags prefixer: (token-prefixer "-L ")
              env-var: "SDL2_LDFLAGS"
              command: "sdl2-config --libs"
              mac-framework: "SDL2")))


;;; If compiling with a Mac framework, register this feature during
;;; compilation so that the correct "#include" statement will be used.
(when (and (eq? 'macosx (software-version))
           (any (cut irregex-search "-framework\\b" <>)
                sdl2-flags-list))
  (set! sdl2-flags-list
    (cons "-feature sdl2-use-mac-framework" sdl2-flags-list)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(define (compile-module module #!optional (extra-flags-list '()))
  (printf "~%COMPILING MODULE: ~A~%~%" module)
  (compile -J -s -O3 -d1 ,@extra-flags-list
           -emit-type-file ,(string-append module ".types")
           ,(string-append module ".scm"))
  (compile -v -s -O3 -d0 ,(string-append module ".import.scm")))


(compile-module "sdl2-features" sdl2-flags-list)
(compile-module "sdl2-internals" sdl2-flags-list)
(compile-module "sdl2")


(define products
  (append-map (lambda (module)
                (list (string-append module ".so")
                      (string-append module ".import.so")
                      (string-append module ".types")))
              (list "sdl2-features" "sdl2-internals" "sdl2")))

(install-extension 'sdl2 products `((version ,version)))
