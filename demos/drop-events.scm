;; The contents of this demo file are made available under the CC0 1.0
;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;; http://creativecommons.org/publicdomain/zero/1.0/


;; Print event info when files or text are dropped onto the window.


(cond-expand
 (chicken-4
  (use (prefix sdl2 sdl2:)
       miscmacros))
 (chicken-5
  (import (chicken condition)
          (chicken format)
          (prefix sdl2 sdl2:)
          miscmacros)))


;;; Initialize the parts of SDL that we need.
(sdl2:set-main-ready!)
(sdl2:init! '(video events))

;; Automatically call sdl2:quit! when program exits normally.
(on-exit sdl2:quit!)

;; Call sdl2:quit! and then call the original exception handler if an
;; unhandled exception reaches the top level.
(current-exception-handler
 (let ((original-handler (current-exception-handler)))
   (lambda (exception)
     (sdl2:quit!)
     (original-handler exception))))


;;; Create a new window.
(define window (sdl2:create-window!
                "Drop files or text on this window"
                'centered 'centered
                640 480))


(define (main-loop)
  (let/cc exit-main-loop!
    (while #t
      (sdl2:pump-events!)
      (while (sdl2:has-events?)
        (handle-event (sdl2:poll-event!) exit-main-loop!)))))

(define (handle-event ev exit-main-loop!)
  (case (sdl2:event-type ev)
    ((quit)
     (exit-main-loop! #t))
    ((key-down)
     (case (sdl2:keyboard-event-sym ev)
       ((escape)
        (exit-main-loop! #t))))
    ((drop-begin)
     (printf "drop-begin (window ~A)~N~!"
             (sdl2:drop-event-window-id ev)))
    ((drop-complete)
     (printf "drop-complete (window ~A)~N~!"
             (sdl2:drop-event-window-id ev)))
    ((drop-file)
     (printf "drop-file (window ~A): ~S~N~!"
             (sdl2:drop-event-window-id ev)
             (sdl2:drop-event-file ev)))
    ((drop-text)
     (printf "drop-text (window ~A): ~S~N~!"
             (sdl2:drop-event-window-id ev)
             (sdl2:drop-event-file ev)))))

(main-loop)
