;;; The contents of this demo file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/

;;; Outputs details for each available render driver on this computer.

(cond-expand
 (chicken-4
  (use (prefix sdl2 sdl2:)))
 (else
  (import (chicken format)
          (prefix sdl2 sdl2:))))

(printf "Number of render drivers: ~A~N" (sdl2:num-render-drivers))

(do ((i 0 (+ i 1)))
    ((= i (sdl2:num-render-drivers)))
  (let* ((window (sdl2:create-window! "" 'undefined 'undefined 1 1))
         (renderer (sdl2:create-renderer! window i))
         (info (sdl2:get-renderer-info renderer)))
    (printf "~NDriver ~A:~N" i)
    (printf "  Name: ~A~N" (sdl2:renderer-info-name info))
    (printf "  Flags: ~A~N" (sdl2:renderer-info-flags info))
    (printf "  Texture formats:~N    ~A~N"
            (sdl2:renderer-info-texture-formats info))
    (printf "  Max texture size: ~A x ~A~N"
            (sdl2:renderer-info-max-texture-width info)
            (sdl2:renderer-info-max-texture-height info))))
