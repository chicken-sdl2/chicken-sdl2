;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;;; This module registers features identifiers based on what version
;;; of SDL and this egg are being used. The feature identifiers can be
;;; checked with the `feature?` function, the `cond-expand` macro, or
;;; the `#+foo` read syntax.
;;;
;;; In compiled code, to check with `cond-expand` or `#+foo`, you must
;;; `(use-for-syntax sdl2)` in CHICKEN 4 or `(import-for-syntax sdl2)`
;;; in CHICKEN 5.
;;;
;;; This is a separate module because the other modules must know the
;;; SDL version at egg compile time, to skip functions and constants
;;; that are not available in the SDL version being used. This module
;;; is compiled first, then imported during compilation of the other
;;; modules.

(module sdl2-features ()

  (import scheme)
  (cond-expand
    (chicken-4
     (import chicken
             foreign))
    (chicken-5
     (import (chicken foreign)
             (chicken platform))))

  (cond-expand
    (sdl2-use-mac-framework
     (foreign-declare "#include <SDL2/SDL.h>"))
    (else
     (foreign-declare "#include \"SDL.h\"")))

  (register-feature! 'sdl2)
  (register-feature! 'sdl2-0.2.0+)
  (register-feature! 'sdl2-0.3.0+)
  (register-feature! 'sdl2-0.4.0+)

  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,0)" bool)
      (register-feature! 'libSDL-2.0.0+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,1)" bool)
      (register-feature! 'libSDL-2.0.1+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,2)" bool)
      (register-feature! 'libSDL-2.0.2+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,3)" bool)
      (register-feature! 'libSDL-2.0.3+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,4)" bool)
      (register-feature! 'libSDL-2.0.4+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,5)" bool)
      (register-feature! 'libSDL-2.0.5+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,6)" bool)
      (register-feature! 'libSDL-2.0.6+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,7)" bool)
      (register-feature! 'libSDL-2.0.7+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,8)" bool)
      (register-feature! 'libSDL-2.0.8+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,9)" bool)
      (register-feature! 'libSDL-2.0.9+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,10)" bool)
      (register-feature! 'libSDL-2.0.10+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,12)" bool)
      (register-feature! 'libSDL-2.0.12+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,14)" bool)
      (register-feature! 'libSDL-2.0.14+))
  (if (foreign-value "SDL_VERSION_ATLEAST(2,0,16)" bool)
      (register-feature! 'libSDL-2.0.16+))
  )
