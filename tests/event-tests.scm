(test-group "register-events!"
  (define pairs (sdl2:register-events! '(foo bar)))

  (test-assert "Returns list of symbol-integer pairs"
    (and (= 2 (length pairs))
         (fixnum? (alist-ref 'foo pairs))
         (fixnum? (alist-ref 'bar pairs))))

  (test-assert "Registers for symbol->event-type conversion"
    (and (= (alist-ref 'foo pairs) (SDL:symbol->event-type 'foo))
         (= (alist-ref 'bar pairs) (SDL:symbol->event-type 'bar))))

  (test-assert "Registers for event-type->symbol conversion"
    (and (eq? 'foo (SDL:event-type->symbol (alist-ref 'foo pairs)))
         (eq? 'bar (SDL:event-type->symbol (alist-ref 'bar pairs))))))
