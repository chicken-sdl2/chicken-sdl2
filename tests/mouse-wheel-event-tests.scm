
(versioned-test-group "sdl2:mouse-wheel-event-direction"
    libSDL-2.0.4+
  (let ((ev (sdl2:make-event 'mouse-wheel)))
    (test "It can be set to 'normal"
          'normal
          (begin
            (sdl2:mouse-wheel-event-direction-set! ev 'normal)
            (sdl2:mouse-wheel-event-direction ev)))

    (test "It can be set to 'flipped"
          'flipped
          (begin
            (sdl2:mouse-wheel-event-direction-set! ev 'flipped)
            (sdl2:mouse-wheel-event-direction ev)))

    (test-error "Signals error if given an unrecognized symbol"
      (sdl2:mouse-wheel-event-direction-set! ev 'foo))))
