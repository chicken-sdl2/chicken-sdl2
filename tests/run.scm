
;;; chicken-sdl2 test suite entry point.

(import scheme)
(cond-expand
  (chicken-4
   (import chicken
           lolevel
           (rename (only extras random)
                   (random random-int)))
   (use irregex
        srfi-1
        srfi-4
        test
        (prefix sdl2-internals SDL:)
        (prefix sdl2 sdl2:)))
  (chicken-5
   (import (chicken base)
           (chicken bitwise)
           (chicken blob)
           (chicken condition)
           (chicken format)
           (chicken irregex)
           (chicken locative)
           (chicken memory)
           (chicken sort)
           (rename (only (chicken random) pseudo-random-integer)
                   (pseudo-random-integer random-int))
           (srfi 1)
           (srfi 4)
           test
           (prefix sdl2-internals SDL:)
           (prefix sdl2 sdl2:))))


(include "test-helpers.scm")


(define Uint8-min   0)
(define Uint8-max   255)
(define Sint32-min -2147483648)
(define Sint32-max  2147483647)


(test-group "sdl2"
  (test-group "general"       (include "general-tests.scm"))
  (test-group "color"         (include "color-tests.scm"))
  (test-group "cursor"        (include "cursor-tests.scm"))
  (test-group "display"       (include "display-tests.scm"))
  (test-group "event"         (include "event-tests.scm"))
  (test-group "game-controller"
    (include "game-controller-tests.scm"))
  (test-group "hints"         (include "hints-tests.scm"))
  (test-group "joystick"      (include "joystick-tests.scm"))
  (test-group "keysym"        (include "keysym-tests.scm"))
  (test-group "mouse-wheel-event"
    (include "mouse-wheel-event-tests.scm"))
  (test-group "palette"       (include "palette-tests.scm"))
  (test-group "point"         (include "point-tests.scm"))
  (test-group "rect"          (include "rect-tests.scm"))
  (test-group "render"        (include "render-tests.scm"))
  (test-group "struct"        (include "struct-tests.scm"))
  (test-group "surface"       (include "surface-tests.scm"))
  (test-group "window"        (include "window-tests.scm"))
  )


(test-exit)
