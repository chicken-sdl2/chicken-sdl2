
(test-group "sdl2:create-cursor"
  (define data-blob #${00000000001ff80000ffff0001ffff8007ffffe00ffffff00ffffff01ffffff83ffffffc3f8ff1fc3f07e0fc7e73ce7e7efbdf7e7ffffffe7ffffffe7ffffffe7ffffffe7ffffffe7ffffffe7c00003e7ddf7bbe3cdf7b3c3edf7b7c3f5f7afc1f8f71f80fe007f00ffffff007ffffe001ffff8000ffff00001ff80000000000})
  (define mask-blob #${001ff80000ffff0001ffff8007ffffe00ffffff01ffffff81ffffff83ffffffc7ffffffe7ffffffe7ffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffffffe7ffffffe7ffffffe3ffffffc1ffffff81ffffff80ffffff007ffffe001ffff8000ffff00001ff800})

  (sdl2:quit!)
  (sdl2:init! '(video))

  (test-assert "Create cursor with a blob"
    (sdl2:cursor? (sdl2:create-cursor
                   data-blob mask-blob 32 32 0 0)))

  (test-assert "Create cursor with a u8vector"
    (sdl2:cursor? (sdl2:create-cursor
                   (blob->u8vector/shared data-blob)
                   (blob->u8vector/shared mask-blob)
                   32 32 0 0)))

  (test-assert "Create cursor with a locative"
    (sdl2:cursor? (sdl2:create-cursor
                   (make-locative data-blob)
                   (make-locative mask-blob)
                   32 32 0 0)))

  (test-assert "Create cursor with a pointer"
    (begin
      (let* ((size (blob-size data-blob))
             (data-ptr (allocate size))
             (mask-ptr (allocate size)))
        (move-memory! data-blob data-ptr size)
        (move-memory! mask-blob mask-ptr size)
        (sdl2:cursor? (sdl2:create-cursor
                       data-ptr mask-ptr 32 32 0 0))))))
