
(test-group "sdl2:get-window-from-id"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (let ((window (sdl2:create-window! "" 0 0 1 1)))
    (test "Returns the window with the given ID"
          window
          (sdl2:get-window-from-id (sdl2:window-id window)))

    (test-error "Signals (exn sdl2) if there is no window with the given ID"
      (sdl2:get-window-from-id (+ 1 (sdl2:window-id window))))

    (sdl2:destroy-window! window)))


;;; These tests are disabled because they are too unreliable. They may
;;; fail or pass depending on the operating system or other factors.
#;
(versioned-test-group "sdl2:grabbed-window"
    libSDL-2.0.4+
  (sdl2:quit!)
  (sdl2:init! '(video))
  (sdl2:pump-events!)

  (test "Returns #f if no window has been opened yet"
        #f
        (sdl2:grabbed-window))

  (let ((window1 (sdl2:create-window! "1" 'centered 'centered 1400 1000))
        (window2 (sdl2:create-window! "2" 'centered 'centered 1400 1000)))

    (test "Returns #f if no window has ever grabbed input"
          #f
          (sdl2:grabbed-window))

    (test "Returns an equal? window if there is a grabbed window"
          window1
          (begin
            ;; Only the window with input focus can grab input.
            (sdl2:raise-window! window1)
            (sdl2:window-grab-set! window1 #t)
            (sdl2:grabbed-window)))

    (test "Returns a different window if the grabbed window changes"
          window2
          (begin
            ;; Only the window with input focus can grab input.
            (sdl2:raise-window! window2)
            (sdl2:window-grab-set! window2 #t)
            (sdl2:grabbed-window)))

    (test "Returns #f if no window has input grab anymore"
          #f
          (begin
            (sdl2:window-grab-set! window2 #f)
            (sdl2:grabbed-window)))

    (sdl2:destroy-window! window1)
    (sdl2:destroy-window! window2)))



(test-group "sdl2:update-window-surface-rects!"
  (let ((window (sdl2:create-window! "" 0 0 10 10)))
    (sdl2:window-surface window)

    (test-assert "Accepts a list of rects"
      (no-error?
       (sdl2:update-window-surface-rects!
        window
        (list (sdl2:make-rect 1 2 3 4)
              (sdl2:make-rect 2 3 4 5)
              (sdl2:make-rect 3 4 5 6)))))

    (test-assert "Accepts a vector of rects"
      (no-error?
       (sdl2:update-window-surface-rects!
        window
        (vector (sdl2:make-rect 1 2 3 4)
                (sdl2:make-rect 2 3 4 5)
                (sdl2:make-rect 3 4 5 6)))))))


(versioned-test-group "sdl2:window-opacity"
    libSDL-2.0.5+
  (define window (sdl2:create-window! "" 0 0 10 10))
  (test "Returns window opacity"
        1.0
        (sdl2:window-opacity window))

  (set! (sdl2:window-opacity window) 0.5)
  (test "Can be set!"
        0.5
        (sdl2:window-opacity window)))


(versioned-test-group "sdl2:window-opacity-set!"
    libSDL-2.0.5+
  (define window (sdl2:create-window! "" 0 0 10 10))
  (sdl2:window-opacity-set! window 0.5)
  (test "Sets window opacity"
        0.5
        (sdl2:window-opacity window)))


(test-group "sdl2:window-resizable?"
  (let ((window (sdl2:create-window! "" 0 0 10 10 '(resizable))))
    (test "Returns #t if window is resizable"
          #t
          (sdl2:window-resizable? window)))

  (let ((window (sdl2:create-window! "" 0 0 10 10)))
    (test "Returns #f if window is not resizable"
          #f
          (sdl2:window-resizable? window)))

  #+libSDL-2.0.5+
  (let ((window (sdl2:create-window! "" 0 0 10 10)))
    (set! (sdl2:window-resizable? window) #t)
    (test "Can be set! to #t with SDL 2.0.5+"
          #t
          (sdl2:window-resizable? window)))

  #+libSDL-2.0.5+
  (let ((window (sdl2:create-window! "" 0 0 10 10 '(resizable))))
    (set! (sdl2:window-resizable? window) #f)
    (test "Can be set! to #f with SDL 2.0.5+"
          #f
          (sdl2:window-resizable? window))))


(versioned-test-group "sdl2:window-resizable-set!"
    libSDL-2.0.5+
  (let ((window (sdl2:create-window! "" 0 0 10 10)))
    (sdl2:window-resizable-set! window #t)
    (test "Can set to #t with SDL 2.0.5+"
          #t
          (sdl2:window-resizable? window)))

  (let ((window (sdl2:create-window! "" 0 0 10 10 '(resizable))))
    (sdl2:window-resizable-set! window #f)
    (test "Can set to #f with SDL 2.0.5+"
          #f
          (sdl2:window-resizable? window))))
