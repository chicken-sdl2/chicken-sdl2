
(test-group "sdl2:get-hint"
  (sdl2:clear-hints!)

  (test-error "Signals error if symbol name is unrecognized"
    (sdl2:get-hint 'foo))

  (test "Returns #f if the hint is not set (symbol name)"
        #f
        (sdl2:get-hint 'render-scale-quality))

  (sdl2:set-hint! 'render-scale-quality "best")
  (test "returns the hint's value if the hint is set (symbol name)"
        "best"
        (sdl2:get-hint 'render-scale-quality))

  (test "returns #f if the hint is not set (string name)"
        #f
        (sdl2:get-hint "foo"))

  (sdl2:set-hint! "foo" "bar")
  (test "returns the hint's value if the hint is set (string name)"
        "bar"
        (sdl2:get-hint "foo")))



(versioned-test-group "sdl2:get-hint-boolean"
    libSDL-2.0.5+
  (sdl2:clear-hints!)

  (test-error "Signals error if symbol name is unrecognized"
    (sdl2:get-hint-boolean 'foo))

  (test-group "hint not set (symbol name)"
    (test "Returns #f (no default)"
          #f
          (sdl2:get-hint-boolean 'framebuffer-acceleration))

    (test "Returns given default (#f)"
          #f
          (sdl2:get-hint-boolean 'framebuffer-acceleration #f))

    (test "Returns given default (#t)"
          #t
          (sdl2:get-hint-boolean 'framebuffer-acceleration #t)))

  (test-group "hint is set to truthy (symbol name)"
    (sdl2:set-hint! 'framebuffer-acceleration "1")
    (test "Returns #t (no default)"
          #t
          (sdl2:get-hint-boolean 'framebuffer-acceleration))
    (test "Returns #t (given default #t)"
          #t
          (sdl2:get-hint-boolean 'framebuffer-acceleration #t))
    (test "Returns #t (given default #f)"
          #t
          (sdl2:get-hint-boolean 'framebuffer-acceleration #f)))

  (test-group "hint is set to falsey (symbol name)"
    (sdl2:set-hint! 'framebuffer-acceleration "0")
    (test "Returns #f (no default)"
          #f
          (sdl2:get-hint-boolean 'framebuffer-acceleration))
    (test "Returns #f (given default #t)"
          #f
          (sdl2:get-hint-boolean 'framebuffer-acceleration #t))
    (test "Returns #f (given default #f)"
          #f
          (sdl2:get-hint-boolean 'framebuffer-acceleration #f)))


  (test-group "hint not set (string name)"
    (test "Returns #f (no default)"
          #f
          (sdl2:get-hint-boolean "foo"))

    (test "Returns given default (#f)"
          #f
          (sdl2:get-hint-boolean "foo" #f))

    (test "Returns given default (#t)"
          #t
          (sdl2:get-hint-boolean "foo" #t)))

  (test-group "hint is set to truthy (string name)"
    (sdl2:set-hint! "foo" "1")
    (test "Returns #t (no default)"
          #t
          (sdl2:get-hint-boolean "foo"))
    (test "Returns #t (given default #t)"
          #t
          (sdl2:get-hint-boolean "foo" #t))
    (test "Returns #t (given default #f)"
          #t
          (sdl2:get-hint-boolean "foo" #f)))

  (test-group "hint is set to falsey (string name)"
    (sdl2:set-hint! "foo" "0")
    (test "Returns #f (no default)"
          #f
          (sdl2:get-hint-boolean "foo"))
    (test "Returns #f (given default #t)"
          #f
          (sdl2:get-hint-boolean "foo" #t))
    (test "Returns #f (given default #f)"
          #f
          (sdl2:get-hint-boolean "foo" #f))))



(test-group "sdl2:set-hint!"
  (sdl2:clear-hints!)

  (test "Returns #t if the hint was set"
        (list #t
              "0")
        (list (sdl2:set-hint! 'render-scale-quality "0" 'default)
              (sdl2:get-hint  'render-scale-quality)))

  (sdl2:set-hint! 'render-scale-quality "0" 'normal)
  (test "Returns #f if the hint was not set"
        #f
        (sdl2:set-hint! 'render-scale-quality "1" 'default))

  (sdl2:clear-hints!)
  (sdl2:set-hint! 'render-scale-quality "0" 'default)

  (test "Sets hint if given priority equal to current"
        (list #t
              "1")
        (list (sdl2:set-hint! 'render-scale-quality "1" 'default)
              (sdl2:get-hint  'render-scale-quality)))

  (test "Sets hint if given priority higher than current"
        (list #t
              "2"
              #t
              "3")
        (list (sdl2:set-hint! 'render-scale-quality "2" 'normal)
              (sdl2:get-hint  'render-scale-quality)
              (sdl2:set-hint! 'render-scale-quality "3" 'override)
              (sdl2:get-hint  'render-scale-quality)))

  (test "Does not set hint if given priority lower than current"
        (list #f
              #f
              "3")
        (list (sdl2:set-hint! 'render-scale-quality "4" 'default)
              (sdl2:set-hint! 'render-scale-quality "4" 'normal)
              (sdl2:get-hint  'render-scale-quality)))

  (sdl2:clear-hints!)

  (test "Priority defaults to normal"
        (list #t
              #f
              #t
              #t)
        (list (sdl2:set-hint! 'render-scale-quality "5")
              (sdl2:set-hint! 'render-scale-quality "5" 'default)
              (sdl2:set-hint! 'render-scale-quality "5" 'normal)
              (sdl2:set-hint! 'render-scale-quality "5" 'override)))

  (test-error "Signals error if value is not a string"
    (sdl2:set-hint! 'render-scale-quality 'best))

  (test-error "Signals error if hint name is unrecognized symbol"
    (sdl2:set-hint! 'foo "0"))

  (test "Accepts arbitrary string as hint name"
        (list #t
              "bar")
        (list (sdl2:set-hint! "foo" "bar")
              (sdl2:get-hint  "foo"))))



(test-group "sdl2:clear-hints!"
  (sdl2:set-hint! 'render-scale-quality "best" 'override)
  (sdl2:set-hint! "foo" "buz" 'override)

  (sdl2:clear-hints!)

  (test "Clears the value of all hints"
        (list #f
              #f)
        (list (sdl2:get-hint 'render-scale-quality)
              (sdl2:get-hint "foo")))

  (test-assert "Clears the priority of all hints"
    ;; This was previously set with priority 'override, so setting
    ;; with 'default could only succeed if old priority was cleared.
    (sdl2:set-hint! 'render-scale-quality "best" 'default)))
