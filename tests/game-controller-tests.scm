
;;; Return the index of the first joystick that is a controller, or #f
;;; if no controllers are detected.
(define (find-controller-index)
  (sdl2:init! '(joystick))
  (sdl2:game-controller-update!)
  (let loop ((n (sdl2:num-joysticks))
             (i 0))
    (cond ((>= i n) #f)
          ((sdl2:is-game-controller? i) i)
          (else (loop n (+ i 1))))))

(define index (find-controller-index))

(when (not index)
  (test-assert "No game-controllers detected; skipping tests"
    #t))


(when index
  (test-group "sdl2:game-controller-open!"
    (test-assert "Returns a game-controller"
      (sdl2:game-controller?
       (sdl2:game-controller-open! index))))


  (test-group "sdl2:game-controller-close!"
    (define ctrl (sdl2:game-controller-open! index))

    (test-assert "Closes the game-controller"
      (no-error? (sdl2:game-controller-close! ctrl))))


  (test-group "sdl2:game-controller-from-instance-id"
    (define ctrl (sdl2:game-controller-open! index))
    (define joy (sdl2:game-controller-get-joystick ctrl))

    (test-assert "Returns the expected game-controller"
      (let* ((id (sdl2:joystick-instance-id joy))
             (ctrl2 (sdl2:game-controller-from-instance-id id)))
        (and (sdl2:game-controller? ctrl2)
             (sdl2:struct-eq? ctrl ctrl2)))))


  (test-group "sdl2:game-controller-update!"
    (test-assert "Doesn't error"
      (no-error? (sdl2:game-controller-update!))))


  (test-group "sdl2:game-controller-event-state?"
    (test-assert "Returns controller event state as a boolean"
      (boolean? (sdl2:game-controller-event-state?)))

    (test "Can be set! to #f"
          #f
          (begin (set! (sdl2:game-controller-event-state?) #f)
                 (sdl2:game-controller-event-state?)))
    (test "Can be set! to #t"
          #t
          (begin (set! (sdl2:game-controller-event-state?) #t)
                 (sdl2:game-controller-event-state?))))


  (test-group "sdl2:game-controller-event-state-set!"
    (test "Can set to #f"
          #f
          (begin (sdl2:game-controller-event-state-set! #f)
                 (sdl2:game-controller-event-state?)))
    (test "Can set to #t"
          #t
          (begin (sdl2:game-controller-event-state-set! #t)
                 (sdl2:game-controller-event-state?))))


  (test-group "sdl2:game-controller-attached?"
    (define ctrl (sdl2:game-controller-open! index))

    (test "Returns #t if controller is attached"
          #t
          (sdl2:game-controller-attached? ctrl)))

  (test-group "sdl2:game-controller-get-joystick"
    (define ctrl (sdl2:game-controller-open! index))

    (test-assert "Returns a sdl2:joystick record"
      (sdl2:joystick? (sdl2:game-controller-get-joystick ctrl))))


  (test-group "sdl2:is-game-controller?"
    (test "Returns #t if joystick index is a controller"
          #t
          (sdl2:is-game-controller? index))

    (test "Returns #f if joystick index is not a controller"
          #f
          (sdl2:is-game-controller? 255)))


  (test-group "sdl2:game-controller-mapping"
    (define ctrl (sdl2:game-controller-open! index))

    (test-assert "Returns a mapping string"
      (string? (sdl2:game-controller-mapping ctrl))))


  (test-group "sdl2:game-controller-mapping-for-guid"
    (define ctrl (sdl2:game-controller-open! index))
    (define joy (sdl2:game-controller-get-joystick ctrl))
    (define guid (sdl2:joystick-get-guid joy))

    (test-assert "Returns the expected mapping string"
      (string=? (sdl2:game-controller-mapping-for-guid guid)
                (sdl2:game-controller-mapping ctrl)))

    (test "Returns #f if GUID has no mapping"
          #f
          (sdl2:game-controller-mapping-for-guid
           (sdl2:joystick-get-guid-from-string
            "45678901234567890123456789012123"))))


  (versioned-test-group "sdl2:game-controller-mapping-for-device-index"
      libSDL-2.0.9+
    (define ctrl (sdl2:game-controller-open! index))

    (test-assert "Returns the expected mapping string"
      (string=? (sdl2:game-controller-mapping-for-device-index index)
                (sdl2:game-controller-mapping ctrl)))

    (test "Returns #f if device index has no mapping"
          #f
          (sdl2:game-controller-mapping-for-device-index 999)))


  (test-group "sdl2:game-controller-add-mapping!"
    (define ctrl (sdl2:game-controller-open! index))
    (define joy (sdl2:game-controller-get-joystick ctrl))
    (define guid (sdl2:joystick-get-guid joy))
    (define guid-str (sdl2:joystick-get-guid-string guid))
    (define name (sdl2:game-controller-name ctrl))
    (define mapping (string-append guid-str "," name ",a:b9" ))

    (test "Returns 1 if a new mapping is added"
          1
          (sdl2:game-controller-add-mapping!
           "12345678901234567890123456789012,Bla,a:b1"))

    (test "Returns 0 if existing mapping is updated"
          0
          (sdl2:game-controller-add-mapping! mapping)))


  (test-group "sdl2:game-controller-add-mappings-from-file!"
    (test "Returns number of mappings added"
          2
          (sdl2:game-controller-add-mappings-from-file!
           "gamecontrollersdb.txt")))


  (test-group "sdl2:game-controller-add-mappings-from-rw!"
    (test "Returns number of mappings added"
          2
          (sdl2:game-controller-add-mappings-from-rw!
           (sdl2:rw-from-string "
# More fake controllers
00034567890121234567890123456789,Foo2,a:b1,platform:Android,
00067890123456789012345678901234,Bar2,a:b2,platform:Android,
00024567890121234567890123456789,Foo2,a:b1,platform:iOS,
00076890123456789012345678901234,Bar2,a:b2,platform:iOS,
00035467890121234567890123456789,Foo2,a:b1,platform:Linux,
00067980123456789012345678901234,Bar2,a:b2,platform:Linux,
00034576890121234567890123456789,Foo2,a:b1,platform:Mac OS X,
00067891023456789012345678901234,Bar2,a:b2,platform:Mac OS X,
00034567980121234567890123456789,Foo2,a:b1,platform:Windows,
00067890132456789012345678901234,Bar2,a:b2,platform:Windows,
")
           #t)))


  (test-group "sdl2:game-controller-name"
    (define ctrl (sdl2:game-controller-open! index))
    (test-assert "Returns controller name as a string"
      (string? (sdl2:game-controller-name ctrl))))


  (test-group "sdl2:game-controller-name-for-index"
    (test-assert "Returns controller name as a string"
      (string? (sdl2:game-controller-name-for-index index))))


  (test-group "sdl2:game-controller-get-axis"
    (define ctrl (sdl2:game-controller-open! index))
    (test-assert "Returns value of axis"
      (integer? (sdl2:game-controller-get-axis ctrl 'left-y))))


  (test-group "sdl2:game-controller-get-axis-from-string"
    (test "Returns enum symbol for specified axis"
          'right-x
          (sdl2:game-controller-get-axis-from-string "rightx")))


  (test-group "sdl2:game-controller-get-axis-from-string-raw"
    (test "Returns integer for specified axis"
          SDL:SDL_CONTROLLER_AXIS_RIGHTX
          (sdl2:game-controller-get-axis-from-string-raw "rightx")))


  (test-group "sdl2:game-controller-get-string-for-axis"
    (test "Returns string for specified axis (symbol)"
          "rightx"
          (sdl2:game-controller-get-string-for-axis 'right-x))

    (test "Returns string for specified axis (integer)"
          "rightx"
          (sdl2:game-controller-get-string-for-axis
           SDL:SDL_CONTROLLER_AXIS_RIGHTX)))


  (test-group "sdl2:game-controller-get-button"
    (define ctrl (sdl2:game-controller-open! index))
    (test-assert "Returns value of button"
      (boolean? (sdl2:game-controller-get-button ctrl 'x))))


  (test-group "sdl2:game-controller-get-button-from-string"
    (test "Returns enum symbol for specified button"
          'start
          (sdl2:game-controller-get-button-from-string "start")))


  (test-group "sdl2:game-controller-get-button-from-string-raw"
    (test "Returns integer for specified button"
          SDL:SDL_CONTROLLER_BUTTON_START
          (sdl2:game-controller-get-button-from-string-raw "start")))


  (test-group "sdl2:game-controller-get-string-for-button"
    (test "Returns string for specified button (symbol)"
          "start"
          (sdl2:game-controller-get-string-for-button 'start))

    (test "Returns string for specified button (integer)"
          "start"
          (sdl2:game-controller-get-string-for-button
           SDL:SDL_CONTROLLER_BUTTON_START)))


  (test-group "sdl2:game-controller-get-bind-for-axis"
    (define ctrl (sdl2:game-controller-open! index))
    (test-assert "Returns a sdl2:game-controller-button-bind"
      (sdl2:game-controller-button-bind?
       (sdl2:game-controller-get-bind-for-axis ctrl 'trigger-left))))

  (test-group "sdl2:game-controller-get-bind-for-button"
    (define ctrl (sdl2:game-controller-open! index))
    (test-assert "Returns a sdl2:game-controller-button-bind"
      (sdl2:game-controller-button-bind?
       (sdl2:game-controller-get-bind-for-button ctrl 'back))))


  (versioned-test-group "sdl2:game-controller-get-vendor"
      libSDL-2.0.6+
    (define ctrl (sdl2:game-controller-open! index))
    (test-assert "Returns USB vendor ID as an integer"
      (integer? (sdl2:game-controller-get-vendor ctrl))))

  (versioned-test-group "sdl2:game-controller-get-product"
      libSDL-2.0.6+
    (define ctrl (sdl2:game-controller-open! index))
    (test-assert "Returns USB product ID as an integer"
      (integer? (sdl2:game-controller-get-product ctrl))))

  (versioned-test-group "sdl2:game-controller-get-product-version"
      libSDL-2.0.6+
    (define ctrl (sdl2:game-controller-open! index))
    (test-assert "Returns USB product version as an integer"
      (integer? (sdl2:game-controller-get-product-version ctrl))))

  (versioned-test-group "sdl2:game-controller-get-player-index"
      libSDL-2.0.9+
    (define ctrl (sdl2:game-controller-open! index))
    (test-assert "Returns controller player index as an integer"
      (integer? (sdl2:game-controller-get-player-index ctrl))))


  (versioned-test-group "sdl2:game-controller-num-mappings"
      libSDL-2.0.6+
    (test-assert "Returns number of mappings available"
      (integer? (sdl2:game-controller-num-mappings))))

  (versioned-test-group "sdl2:game-controller-mapping-for-index"
      libSDL-2.0.6+
    ;; Make sure there is at least 1 mapping
    (sdl2:game-controller-add-mapping!
     "12345678901234567890123456789012,Bla,a:b1")

    (test-assert "Returns mapping as a string"
      (string? (sdl2:game-controller-mapping-for-index 0)))

    (test "Returns #f if there is no mapping"
          #f
          (sdl2:game-controller-mapping-for-index
           (+ 1 (sdl2:game-controller-num-mappings)))))


  (versioned-test-group "sdl2:game-controller-rumble!"
      libSDL-2.0.9+
    (define ctrl (sdl2:game-controller-open! index))
    (test-assert "Returns boolean for whether rumble was supported"
      (boolean? (sdl2:game-controller-rumble! ctrl #xFFFF #xFFFF 1000))))

  )
