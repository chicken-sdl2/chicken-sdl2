
(when (= 0 (sdl2:num-joysticks))
  (test-assert "No joysticks detected; skipping tests"
    #t))


(when (< 0 (sdl2:num-joysticks))
  (test-group "sdl2:joystick-open!"
    (test-assert "Returns a joystick"
      (sdl2:joystick?
       (sdl2:joystick-open! 0))))


  (test-group "sdl2:joystick-close!"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Closes the joystick"
      (no-error? (sdl2:joystick-close! joy))))


  (test-group "sdl2:joystick-from-instance-id"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns the expected joystick"
      (let* ((id (sdl2:joystick-instance-id joy))
             (joy2 (sdl2:joystick-from-instance-id id)))
        (and (sdl2:joystick? joy2)
             (sdl2:struct-eq? joy joy2)))))


  (test-group "sdl2:joystick-update!"
    (test-assert "Doesn't error"
      (no-error? (sdl2:joystick-update!))))


  (test-group "sdl2:joystick-event-state"
    (test-assert "Returns controller event state as a boolean"
      (boolean? (sdl2:joystick-event-state)))

    (test "Can be set! to #f"
          #f
          (begin (set! (sdl2:joystick-event-state) #f)
                 (sdl2:joystick-event-state)))
    (test "Can be set! to #t"
          #t
          (begin (set! (sdl2:joystick-event-state) #t)
                 (sdl2:joystick-event-state))))


  (test-group "sdl2:joystick-event-state-set!"
    (test "Can set to #f"
          #f
          (begin (sdl2:joystick-event-state-set! #f)
                 (sdl2:joystick-event-state)))
    (test "Can set to #t"
          #t
          (begin (sdl2:joystick-event-state-set! #t)
                 (sdl2:joystick-event-state))))


  (test-group "sdl2:joystick-attached?"
    (define joy (sdl2:joystick-open! 0))

    (test "Returns #t if controller is attached"
          #t
          (sdl2:joystick-attached? joy)))


  (versioned-test-group "sdl2:joystick-current-power-level"
      libSDL-2.0.4+
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns power level as a symbol"
      (symbol? (sdl2:joystick-current-power-level joy))))


  (test-group "sdl2:joystick-num-axes"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns number of axes"
      (integer? (sdl2:joystick-num-axes joy))))

  (test-group "sdl2:joystick-num-balls"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns number of balls"
      (integer? (sdl2:joystick-num-balls joy))))

  (test-group "sdl2:joystick-num-buttons"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns number of buttons"
      (integer? (sdl2:joystick-num-buttons joy))))

  (test-group "sdl2:joystick-num-hats"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns number of hats"
      (integer? (sdl2:joystick-num-hats joy))))


  (test-group "sdl2:joystick-get-axis"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns value of the axis as an integer"
      (integer? (sdl2:joystick-get-axis joy 0))))

  (test-group "sdl2:joystick-get-ball"
    (define joy (sdl2:joystick-open! 0))
    (case (sdl2:joystick-num-balls joy)
      ((0)
       (test-assert "Joystick 0 has no ball; skipping test"
         #t))
      (else
       (test-assert "Returns x and y values of the ball as integers"
         (let-values (((x y . more) (sdl2:joystick-get-ball joy 0)))
           (and (integer? x)
                (integer? y)
                (null? more)))))))

  (test-group "sdl2:joystick-get-button"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns value of the button as a boolean"
      (boolean? (sdl2:joystick-get-button joy 0))))

  (test-group "sdl2:joystick-get-hat"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns direction of the hat as a symbol"
      (symbol? (sdl2:joystick-get-hat joy 0))))

  (test-group "sdl2:joystick-get-hat-raw"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns direction of the hat as an integer"
      (integer? (sdl2:joystick-get-hat-raw joy 0))))


  (test-group "sdl2:joystick-name"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns controller name as a string"
      (string? (sdl2:joystick-name joy))))

  (test-group "sdl2:joystick-name-for-index"
    (test-assert "Returns controller name as a string"
      (string? (sdl2:joystick-name-for-index 0))))


  (test-group "sdl2:joystick-get-device-guid"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns GUID for the specified index"
      (string=? (sdl2:joystick-get-guid-string
                 (sdl2:joystick-get-device-guid 0))
                (sdl2:joystick-get-guid-string
                 (sdl2:joystick-get-guid joy)))))

  (test-group "sdl2:joystick-get-device-guid"
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns GUID for the given joystick"
      (sdl2:joystick-guid? (sdl2:joystick-get-guid joy))))

  (test-group "sdl2:joystick-get-guid-from-string"
    (test-assert "Returns GUID with the given value"
      (sdl2:joystick-guid?
       (sdl2:joystick-get-guid-from-string
        "01234567890121234567890123456789"))))

  (test-group "sdl2:joystick-get-guid-from-string"
    (test-assert "Returns string of the given GUID"
      (string=? "01234567890121234567890123456789"
                (sdl2:joystick-get-guid-string
                 (sdl2:joystick-get-guid-from-string
                  "01234567890121234567890123456789")))))


  (versioned-test-group "sdl2:joystick-get-device-vendor"
      libSDL-2.0.6+
    (test-assert "Returns USB vendor ID as an integer"
      (integer? (sdl2:joystick-get-device-vendor 0))))

  (versioned-test-group "sdl2:joystick-get-device-product"
      libSDL-2.0.6+
    (test-assert "Returns USB product ID as an integer"
      (integer? (sdl2:joystick-get-device-product 0))))

  (versioned-test-group "sdl2:joystick-get-device-product-version"
      libSDL-2.0.6+
    (test-assert "Returns USB product version as an integer"
      (integer? (sdl2:joystick-get-device-product-version 0))))

  (versioned-test-group "sdl2:joystick-get-device-type"
      libSDL-2.0.6+
    (test-assert "Returns joystick type as a symbol"
      (symbol? (sdl2:joystick-get-device-type 0))))

  (versioned-test-group "sdl2:joystick-get-device-type-raw"
      libSDL-2.0.6+
    (test-assert "Returns joystick type as an integer"
      (integer? (sdl2:joystick-get-device-type-raw 0))))

  (versioned-test-group "sdl2:joystick-get-device-instance-id"
      libSDL-2.0.6+
    (test-assert "Returns instance ID as an integer"
      (integer? (sdl2:joystick-get-device-instance-id 0))))

  (versioned-test-group "sdl2:joystick-get-device-player-index"
      libSDL-2.0.9+
    (test-assert "Returns player index as an integer"
      (integer? (sdl2:joystick-get-device-player-index 0))))


  (versioned-test-group "sdl2:joystick-get-vendor"
      libSDL-2.0.6+
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns USB vendor ID as an integer"
      (integer? (sdl2:joystick-get-vendor joy))))

  (versioned-test-group "sdl2:joystick-get-product"
      libSDL-2.0.6+
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns USB product ID as an integer"
      (integer? (sdl2:joystick-get-product joy))))

  (versioned-test-group "sdl2:joystick-get-product-version"
      libSDL-2.0.6+
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns USB product version as an integer"
      (integer? (sdl2:joystick-get-product-version joy))))

  (versioned-test-group "sdl2:joystick-get-type"
      libSDL-2.0.6+
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns joystick type as a symbol"
      (symbol? (sdl2:joystick-get-type joy))))

  (versioned-test-group "sdl2:joystick-get-type-raw"
      libSDL-2.0.6+
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns joystick type as an integer"
      (integer? (sdl2:joystick-get-type-raw joy))))

  (versioned-test-group "sdl2:joystick-get-player-index"
      libSDL-2.0.9+
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns joystick player index as an integer"
      (integer? (sdl2:joystick-get-player-index joy))))


  (versioned-test-group "sdl2:joystick-get-axis-initial-state"
      libSDL-2.0.6+
    (define joy (sdl2:joystick-open! 0))
    (test-assert "First return value tells what the initial state is"
      (integer? (nth-value 0 (sdl2:joystick-get-axis-initial-state joy 0))))
    (test-assert "Second return value tells whether there is initial state"
      (boolean? (nth-value 1 (sdl2:joystick-get-axis-initial-state joy 0)))))


  (versioned-test-group "sdl2:lock-joysticks!"
      libSDL-2.0.7+
    (test-assert "Doesn't error"
      (no-error? (sdl2:lock-joysticks!))))

  (versioned-test-group "sdl2:unlock-joysticks!"
      libSDL-2.0.7+
    (test-assert "Doesn't error"
      (no-error? (sdl2:unlock-joysticks!))))


  (versioned-test-group "sdl2:joystick-rumble!"
      libSDL-2.0.9+
    (define joy (sdl2:joystick-open! 0))
    (test-assert "Returns boolean for whether rumble was supported"
      (boolean? (sdl2:joystick-rumble! joy #xFFFF #xFFFF 1000))))

  )
