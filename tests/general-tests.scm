
(test-group "sdl2:get-error / sdl2:set-error! / sdl2:clear-error!"
  (test "Can get message that was set"
        "Error! Error! Error!"
        (begin
          (sdl2:set-error! "Error! Error! Error!")
          (sdl2:get-error)))

  (test "Can clear error message"
        ""
        (begin
          (sdl2:set-error! "Error! Error! Error!")
          (sdl2:clear-error!)
          (sdl2:get-error))))


(versioned-test-group "sdl2:get-base-path"
    libSDL-2.0.1+
  (test-assert "Returns a string"
    (< 0 (string-length (sdl2:get-base-path)))))


(versioned-test-group "sdl2:get-pref-path"
    libSDL-2.0.1+
  (define org (sprintf "MyOrgName~A" (random-int 10000)))
  (define app (sprintf "MyAppName~A" (random-int 10000)))

  (test-assert "Returns a string"
    (< 0 (string-length (sdl2:get-pref-path org app))))

  (test-assert "Includes the given org string"
    (irregex-search org (sdl2:get-pref-path org app)))

  (test-assert "Includes the given app string"
    (irregex-search app (sdl2:get-pref-path org app))))


(versioned-test-group "sdl2:is-tablet?"
    libSDL-2.0.9+
  (test-assert "Returns a boolean"
    (boolean? (sdl2:is-tablet?))))


(test-group "get-power-info"
  (let-values (((state secs pct) (sdl2:get-power-info)))
    (test-assert "First value is a power state symbol"
      (memq state '(unknown on-battery no-battery charging charged)))

    (test-assert "Second value is an integer >= -1"
      (and (integer? secs)
           (<= -1 secs)))

    (test-assert "Third value is an integer between -1 and 100"
      (and (integer? pct)
           (<= -1 pct 100)))))
