
(test-group "video-init!"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (cond
   ((= 0 (sdl2:get-num-video-drivers))
    (test-assert "No video drivers available; skipping tests" #t))
   (else
    (test-assert "Can be called with no args for default driver"
      (no-error? (sdl2:video-init!)))

    (test-assert "Can be called with #f for default driver"
      (no-error? (sdl2:video-init! #f)))

    (test-assert "Can be called with driver name string"
      (no-error? (sdl2:video-init! (sdl2:get-video-driver 0)))))))


(test-group "video-quit!"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (test-assert "Does not raise error"
    (no-error? (sdl2:video-quit!))))


(test-group "get-num-video-drivers"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (test-assert "Returns an integer"
    (integer? (sdl2:get-num-video-drivers))))


(test-group "get-video-driver"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (cond
   ((= 0 (sdl2:get-num-video-drivers))
    (test-assert "No video drivers available; skipping tests" #t))
   (else
    (test-assert "Returns driver name as a string"
      (string? (sdl2:get-video-driver 0))))))


(test-group "get-current-video-driver"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (cond
   ((= 0 (sdl2:get-num-video-drivers))
    (test-assert "No video drivers available; skipping tests" #t))
   (else
    (let ((driver (sdl2:get-video-driver 0)))
      (test "Returns string name of video driver"
            driver
            (begin
              (sdl2:video-init! driver)
              (sdl2:get-current-video-driver))))

    (test "Returns #f if no driver is initialized"
          #f
          (begin
            (sdl2:video-quit!)
            (sdl2:get-current-video-driver))))))


(test-group "get-num-video-displays"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (test-assert "Returns an integer"
    (integer? (sdl2:get-num-video-displays))))


(test-group "get-display-name"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (cond
   ((= 0 (sdl2:get-num-video-displays))
    (test-assert "No video displays available; skipping tests" #t))
   (else
    (test-assert "Returns display name as a string"
      (string? (sdl2:get-display-name 0))))))


(versioned-test-group "get-display-dpi"
    libSDL-2.0.4+
  (sdl2:quit!)
  (sdl2:init! '(video))

  (cond
   ((= 0 (sdl2:get-num-video-displays))
    (test-assert "No video displays available; skipping tests" #t))
   (else
    (test-assert "Returns diagonal, horizontal, and vertical DPIs"
      (let-values (((ddpi hdpi vdpi) (sdl2:get-display-dpi 0)))
        (and (flonum? ddpi)
             (flonum? hdpi)
             (flonum? vdpi)))))))


(test-group "get-display-bounds"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (test-assert "Returns a new rect if given no rect"
    (sdl2:rect? (sdl2:get-display-bounds 0)))

  (test-assert "Modifies and returns given rect"
    (let* ((rect (sdl2:make-rect))
           (result (sdl2:get-display-bounds 0 rect)))
      (and (sdl2:rect? result)
           (sdl2:struct-eq? rect result)))))


(versioned-test-group "get-display-usable-bounds"
    libSDL-2.0.5+
  (sdl2:quit!)
  (sdl2:init! '(video))

  (test-assert "Returns a new rect if given no rect"
    (sdl2:rect? (sdl2:get-display-usable-bounds 0)))

  (test-assert "Modifies and returns given rect"
    (let* ((rect (sdl2:make-rect))
           (result (sdl2:get-display-usable-bounds 0 rect)))
      (and (sdl2:rect? result)
           (sdl2:struct-eq? rect result)))))


(test-group "screen-saver-enabled? / screen-saver-enabled-set!"
  (sdl2:screen-saver-enabled-set! #f)
  (test "Can enable screen saver"
        #t
        (begin (sdl2:screen-saver-enabled-set! #t)
               (sdl2:screen-saver-enabled?)))

  (sdl2:screen-saver-enabled-set! #t)
  (test "Can disable screen saver"
        #f
        (begin (sdl2:screen-saver-enabled-set! #f)
               (sdl2:screen-saver-enabled?))))


(test-group "get-num-display-modes"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (cond
   ((= 0 (sdl2:get-num-video-displays))
    (test-assert "No video displays available; skipping tests" #t))
   (else
    (test-assert "Returns an integer"
      (integer? (sdl2:get-num-display-modes 0))))))


(test-group "get-display-mode"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (cond
   ((= 0 (sdl2:get-num-video-displays))
    (test-assert "No video displays available; skipping tests" #t))
   ((= 0 (sdl2:get-num-display-modes 0))
    (test-assert "No display modes available; skipping tests" #t))
   (else
    (test-assert "Returns a sdl2:display-mode"
      (sdl2:display-mode? (sdl2:get-display-mode 0 0))))))


(test-group "get-current-display-mode"
  (sdl2:quit!)
  (sdl2:init! '(video))

  (cond
   ((= 0 (sdl2:get-num-video-displays))
    (test-assert "No video displays available; skipping tests" #t))
   ((= 0 (sdl2:get-num-display-modes 0))
    (test-assert "No display modes available; skipping tests" #t))
   (else
    (test-assert "Returns a sdl2:display-mode"
      (sdl2:display-mode? (sdl2:get-current-display-mode 0))))))


(test-group "get-closest-display-mode"
  (cond
   ((= 0 (sdl2:get-num-video-displays))
    (test-assert "No video displays available; skipping tests" #t))
   ((= 0 (sdl2:get-num-display-modes 0))
    (test-assert "No display modes available; skipping tests" #t))
   (else
    (test-assert "Returns a sdl2:display-mode"
      (let ((target-mode (sdl2:make-display-mode 'rgb888 640 480 60)))
        (sdl2:display-mode? (sdl2:get-closest-display-mode
                             0 target-mode)))))))


(test-group "get-desktop-display-mode"
  (cond
   ((= 0 (sdl2:get-num-video-displays))
    (test-assert "No video displays available; skipping tests" #t))
   ((= 0 (sdl2:get-num-display-modes 0))
    (test-assert "No display modes available; skipping tests" #t))
   (else
    (test-assert "Returns a sdl2:display-mode"
      (sdl2:display-mode? (sdl2:get-desktop-display-mode 0))))))


(versioned-test-group "get-display-orientation"
    libSDL-2.0.9+
  (sdl2:quit!)
  (sdl2:init! '(video))

  (test-assert "Returns a symbol"
    (symbol? (sdl2:get-display-orientation 0))))
