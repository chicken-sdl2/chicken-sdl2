;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(module sdl2 ()

(import scheme)
(cond-expand
  (chicken-4
   (import chicken)
   (use extras
        lolevel
        data-structures
        srfi-1
        srfi-4
        sdl2-features
        sdl2-internals)
   (use-for-syntax sdl2-features))
  (chicken-5
   (import (chicken base)
           (chicken bitwise)
           (chicken blob)
           (chicken condition)
           (chicken format)
           (chicken gc)
           (chicken locative)
           (chicken memory)
           (chicken module)
           (chicken platform)
           (chicken type)
           (srfi 1)
           (srfi 4)
           sdl2-features
           sdl2-internals)
   (import-for-syntax sdl2-features)))

(include "lib/version.scm")

(include "lib/shared/types.scm")
(include "lib/shared/reexport-types.scm")

(include "lib/shared/define-versioned.scm")
(include "lib/shared/error-helpers.scm")
(include "lib/shared/with-temp-mem.scm")

(include "lib/sdl2/helpers/try-call.scm")

(include "lib/sdl2/reexports.scm")
(include "lib/sdl2/general.scm")
(include "lib/sdl2/cursor.scm")
(include "lib/sdl2/display.scm")
(include "lib/sdl2/events.scm")
(include "lib/sdl2/game-controller.scm")
(include "lib/sdl2/gl.scm")
(include "lib/sdl2/hints.scm")
(include "lib/sdl2/joystick.scm")
(include "lib/sdl2/keyboard.scm")
(include "lib/sdl2/mouse.scm")
(include "lib/sdl2/palette.scm")
(include "lib/sdl2/pixel-format.scm")
(include "lib/sdl2/rect.scm")
(include "lib/sdl2/renderer.scm")
(include "lib/sdl2/renderer-draw.scm")
(include "lib/sdl2/rwops.scm")
(include "lib/sdl2/surface.scm")
(include "lib/sdl2/texture.scm")
(include "lib/sdl2/timer.scm")
(include "lib/sdl2/touch.scm")
(include "lib/sdl2/vulkan.scm")
(include "lib/sdl2/window.scm")

)
