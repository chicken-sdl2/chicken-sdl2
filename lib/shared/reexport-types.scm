;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;;; This file contains type declarations from sdl2-internals that are
;;; reexported from sdl2. It is included in both modules because type
;;; declarations don't carry over across module reexports.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; AUDIO-CVT

;;; sdl2-internals/record-types/audio-cvt.scm

(: audio-cvt? (any --> boolean : sdl2:audio-cvt))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; AUDIO-SPEC

;;; sdl2-internals/record-types/audio-spec.scm

(: audio-spec? (any --> boolean : sdl2:audio-spec))


;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; COLOR

;;; sdl2-internals/record-types/color.scm

(: color? (any --> boolean : sdl2:color))
(: free-color! (sdl2:color -> void))
(: alloc-color* (--> sdl2:color))
(: alloc-color (--> sdl2:color))

(: colour? (any --> boolean : sdl2:color))
(: free-colour! (sdl2:color -> void))
(: alloc-colour* (--> sdl2:color))
(: alloc-colour (--> sdl2:color))

;;; sdl2-internals/accessors/color.scm

(: color-r (sdl2:color* --> fixnum))
(: color-r-set! (sdl2:color* fixnum -> void))
(: color-g (sdl2:color* --> fixnum))
(: color-g-set! (sdl2:color* fixnum -> void))
(: color-b (sdl2:color* --> fixnum))
(: color-b-set! (sdl2:color* fixnum -> void))
(: color-a (sdl2:color* --> fixnum))
(: color-a-set! (sdl2:color* fixnum -> void))

(: colour-r (sdl2:color* --> fixnum))
(: colour-r-set! (sdl2:color* fixnum -> void))
(: colour-g (sdl2:color* --> fixnum))
(: colour-g-set! (sdl2:color* fixnum -> void))
(: colour-b (sdl2:color* --> fixnum))
(: colour-b-set! (sdl2:color* fixnum -> void))
(: colour-a (sdl2:color* --> fixnum))
(: colour-a-set! (sdl2:color* fixnum -> void))

;;; sdl2-internals/extras/color.scm

(: make-color (#!optional fixnum fixnum fixnum fixnum -> sdl2:color))
(: make-color* (#!optional fixnum fixnum fixnum fixnum -> sdl2:color))
(: color-set! (sdl2:color* #!optional fixnum fixnum fixnum fixnum -> sdl2:color*))
(: color->list (sdl2:color* -> (list fixnum fixnum fixnum fixnum)))
(: color->values (sdl2:color* -> fixnum fixnum fixnum fixnum))
(: color=? (sdl2:color* sdl2:color* -> boolean))
(: color-copy! (sdl2:color* sdl2:color* -> sdl2:color*))
(: color-copy (sdl2:color* -> sdl2:color))
(: color-scale! (sdl2:color* number #!optional sdl2:color* -> sdl2:color*))
(: color-scale (sdl2:color* number -> sdl2:color*))
(: color-mult! (sdl2:color* sdl2:color* #!optional sdl2:color* -> sdl2:color*))
(: color-mult (sdl2:color* sdl2:color* -> sdl2:color*))
(: color-add! (sdl2:color* sdl2:color* #!optional sdl2:color* -> sdl2:color*))
(: color-add (sdl2:color* sdl2:color* -> sdl2:color*))
(: color-sub! (sdl2:color* sdl2:color* #!optional sdl2:color* -> sdl2:color*))
(: color-sub (sdl2:color* sdl2:color* -> sdl2:color*))
(: color-lerp! (sdl2:color* sdl2:color* float #!optional sdl2:color* -> sdl2:color*))
(: color-lerp (sdl2:color* sdl2:color* float -> sdl2:color))

(: make-colour (#!optional fixnum fixnum fixnum fixnum -> sdl2:color))
(: make-colour* (#!optional fixnum fixnum fixnum fixnum -> sdl2:color))
(: colour-set! (sdl2:color* #!optional fixnum fixnum fixnum fixnum -> sdl2:color*))
(: colour->list (sdl2:color* -> (list fixnum fixnum fixnum fixnum)))
(: colour->values (sdl2:color* -> fixnum fixnum fixnum fixnum))
(: colour=? (sdl2:color* sdl2:color* -> boolean))
(: colour-copy! (sdl2:color* sdl2:color* -> sdl2:color*))
(: colour-copy (sdl2:color* -> sdl2:color))
(: colour-scale! (sdl2:color* number #!optional sdl2:color* -> sdl2:color*))
(: colour-scale (sdl2:color* number -> sdl2:color*))
(: colour-mult! (sdl2:color* sdl2:color* #!optional sdl2:color* -> sdl2:color*))
(: colour-mult (sdl2:color* sdl2:color* -> sdl2:color*))
(: colour-add! (sdl2:color* sdl2:color* #!optional sdl2:color* -> sdl2:color*))
(: colour-add (sdl2:color* sdl2:color* -> sdl2:color*))
(: colour-sub! (sdl2:color* sdl2:color* #!optional sdl2:color* -> sdl2:color*))
(: colour-sub (sdl2:color* sdl2:color* -> sdl2:color*))
(: colour-lerp! (sdl2:color* sdl2:color* float #!optional sdl2:color* -> sdl2:color*))
(: colour-lerp (sdl2:color* sdl2:color* float -> sdl2:color))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CURSOR

;;; sdl2-internals/record-types/cursor.scm

(: cursor? (any --> boolean : sdl2:cursor))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DISPLAY-MODE

;;; sdl2-internals/record-types/display-mode.scm

(: display-mode? (any --> boolean : sdl2:display-mode))
(: free-display-mode! (sdl2:display-mode -> void))
(: alloc-display-mode* (--> sdl2:display-mode))
(: alloc-display-mode (--> sdl2:display-mode))

;;; sdl2-internals/accessors/display-mode.scm

(: display-mode-format-raw (sdl2:display-mode* -> Uint32))
(: display-mode-format-raw-set! (sdl2:display-mode* Uint32 -> void))
(: display-mode-format (sdl2:display-mode* -> symbol))
(: display-mode-format-set! (sdl2:display-mode* enum -> void))
(: display-mode-w (sdl2:display-mode* -> Sint32))
(: display-mode-w-set! (sdl2:display-mode* Sint32 -> void))
(: display-mode-h (sdl2:display-mode* -> Sint32))
(: display-mode-h-set! (sdl2:display-mode* Sint32 -> void))
(: display-mode-refresh-rate (sdl2:display-mode* -> Sint32))
(: display-mode-refresh-rate-set! (sdl2:display-mode* Sint32 -> void))
(: make-display-mode (#!optional enum Sint32 Sint32 Sint32 -> sdl2:display-mode))
(: make-display-mode* (#!optional enum Sint32 Sint32 Sint32 -> sdl2:display-mode*))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; EVENT

;;; sdl2-internals/record-types/event.scm

(: event? (any --> boolean : sdl2:event))
(: free-event! (sdl2:event -> void))
(: alloc-event* (--> sdl2:event))
(: alloc-event (--> sdl2:event))

;;; sdl2-internals/accessors/events/common.scm

(: make-event (#!optional enum --> sdl2:event))
(: make-event* (#!optional enum --> sdl2:event))
(: event-type-raw (sdl2:event* --> Sint32))
(: event-type-raw-set! (sdl2:event* Sint32 -> void))
(: event-type (sdl2:event* --> symbol))
(: event-type-set! (sdl2:event* enum -> void))
(: event-timestamp (sdl2:event* --> Uint32))
(: event-timestamp-set! (sdl2:event* Uint32 -> void))

;;; sdl2-internals/accessors/events/controller-axis-event.scm

(: controller-axis-event? (any --> boolean))
(: controller-axis-event-which (sdl2:event* --> SDL_JoystickID))
(: controller-axis-event-which-set! (sdl2:event* SDL_JoystickID -> void))
(: controller-axis-event-axis (sdl2:event* --> symbol))
(: controller-axis-event-axis-raw (sdl2:event* --> Uint8))
(: controller-axis-event-axis-set! (sdl2:event* Uint8 -> void))
(: controller-axis-event-value (sdl2:event* --> Sint16))
(: controller-axis-event-value-set! (sdl2:event* Sint16 -> void))

;;; sdl2-internals/accessors/events/controller-button-event.scm

(: controller-button-event? (any --> boolean))
(: controller-button-event-which (sdl2:event* --> SDL_JoystickID))
(: controller-button-event-which-set! (sdl2:event* SDL_JoystickID -> void))
(: controller-button-event-button (sdl2:event* --> symbol))
(: controller-button-event-button-raw (sdl2:event* --> Uint8))
(: controller-button-event-button-set! (sdl2:event* Uint8 -> void))
(: controller-button-event-state (sdl2:event* --> boolean))
(: controller-button-event-state-set! (sdl2:event* boolean -> void))

;;; sdl2-internals/accessors/events/controller-device-event.scm

(: controller-device-event? (any --> boolean))
(: controller-device-event-which (sdl2:event* --> Sint32))
(: controller-device-event-which-set! (sdl2:event* Sint32 -> void))

;;; sdl2-internals/accessors/events/display-event.scm

#+libSDL-2.0.9+
(begin
  (: display-event? (any --> boolean))
  (: display-event-display (sdl2:event* --> Uint32))
  (: display-event-display-set! (sdl2:event* Uint32 -> void))
  (: display-event-event (sdl2:event* --> symbol))
  (: display-event-event-set! (sdl2:event* enum -> void))
  (: display-event-event-raw (sdl2:event* --> Uint8))
  (: display-event-event-raw-set! (sdl2:event* Uint8 -> void))
  (: display-event-data1 (sdl2:event* --> Sint32))
  (: display-event-data1-set! (sdl2:event* Sint32 -> void))
  (: display-event-orientation (sdl2:event* --> symbol))
  (: display-event-orientation-set! (sdl2:event* enum -> void)))

;;; sdl2-internals/accessors/events/dollar-gesture-event.scm

(: dollar-gesture-event? (any --> boolean))
(: dollar-gesture-event-touch-id (sdl2:event* --> SDL_TouchID))
(: dollar-gesture-event-touch-id-set! (sdl2:event* SDL_TouchID -> void))
(: dollar-gesture-event-gesture-id (sdl2:event* --> SDL_GestureID))
(: dollar-gesture-event-gesture-id-set! (sdl2:event* SDL_GestureID -> void))
(: dollar-gesture-event-num-fingers (sdl2:event* --> Uint32))
(: dollar-gesture-event-num-fingers-set! (sdl2:event* Uint32 -> void))
(: dollar-gesture-event-error (sdl2:event* --> float))
(: dollar-gesture-event-error-set! (sdl2:event* float -> void))
(: dollar-gesture-event-x (sdl2:event* --> float))
(: dollar-gesture-event-x-set! (sdl2:event* float -> void))
(: dollar-gesture-event-y (sdl2:event* --> float))
(: dollar-gesture-event-y-set! (sdl2:event* float -> void))

;;; sdl2-internals/accessors/events/drop-event.scm

#+libSDL-2.0.5+
(begin
  (: drop-event? (any --> boolean))
  (: drop-event-file (sdl2:event* --> string))
  (: drop-event-file-set! (sdl2:event* string -> void))
  (: drop-event-window-id (sdl2:event* --> integer))
  (: drop-event-window-id-set! (sdl2:event* integer -> void)))

;;; sdl2-internals/accessors/events/joy-axis-event.scm

(: joy-axis-event? (any --> boolean))
(: joy-axis-event-which (sdl2:event* --> SDL_JoystickID))
(: joy-axis-event-which-set! (sdl2:event* SDL_JoystickID -> void))
(: joy-axis-event-axis (sdl2:event* --> Uint8))
(: joy-axis-event-axis-set! (sdl2:event* Uint8 -> void))
(: joy-axis-event-value (sdl2:event* --> Sint16))
(: joy-axis-event-value-set! (sdl2:event* Sint16 -> void))

;;; sdl2-internals/accessors/events/joy-ball-event.scm

(: joy-ball-event? (any --> boolean))
(: joy-ball-event-which (sdl2:event* --> SDL_JoystickID))
(: joy-ball-event-which-set! (sdl2:event* SDL_JoystickID -> void))
(: joy-ball-event-ball (sdl2:event* --> Uint8))
(: joy-ball-event-ball-set! (sdl2:event* Uint8 -> void))
(: joy-ball-event-xrel (sdl2:event* --> Sint16))
(: joy-ball-event-xrel-set! (sdl2:event* Sint16 -> void))
(: joy-ball-event-yrel (sdl2:event* --> Sint16))
(: joy-ball-event-yrel-set! (sdl2:event* Sint16 -> void))

;;; sdl2-internals/accessors/events/joy-button-event.scm

(: joy-button-event? (any --> boolean))
(: joy-button-event-which (sdl2:event* --> SDL_JoystickID))
(: joy-button-event-which-set! (sdl2:event* SDL_JoystickID -> void))
(: joy-button-event-button (sdl2:event* --> Uint8))
(: joy-button-event-button-set! (sdl2:event* Uint8 -> void))
(: joy-button-event-state (sdl2:event* --> boolean))
(: joy-button-event-state-set! (sdl2:event* boolean -> void))

;;; sdl2-internals/accessors/events/joy-device-event.scm

(: joy-device-event? (any --> boolean))
(: joy-device-event-which (sdl2:event* --> SDL_JoystickID))
(: joy-device-event-which-set! (sdl2:event* SDL_JoystickID -> void))

;;; sdl2-internals/accessors/events/joy-hat-event.scm

(: joy-hat-event? (any --> boolean))
(: joy-hat-event-which (sdl2:event* --> SDL_JoystickID))
(: joy-hat-event-which-set! (sdl2:event* SDL_JoystickID -> void))
(: joy-hat-event-hat (sdl2:event* --> Uint8))
(: joy-hat-event-hat-set! (sdl2:event* Uint8 -> void))
(: joy-hat-event-value-raw (sdl2:event* --> Uint8))
(: joy-hat-event-value-raw-set! (sdl2:event* Uint8 -> void))
(: joy-hat-event-value (sdl2:event* --> symbol))
(: joy-hat-event-value-set! (sdl2:event* enum -> void))

;;; sdl2-internals/accessors/events/keyboard-event.scm

(: keyboard-event? (any --> boolean))
(: keyboard-event-window-id (sdl2:event* --> Uint32))
(: keyboard-event-window-id-set! (sdl2:event* Uint32 -> void))
(: keyboard-event-state (sdl2:event* --> boolean))
(: keyboard-event-state-set! (sdl2:event* boolean -> void))
(: keyboard-event-repeat (sdl2:event* --> Uint8))
(: keyboard-event-repeat-set! (sdl2:event* Uint8 -> void))
(: keyboard-event-keysym (sdl2:event* --> sdl2:keysym))
(: keyboard-event-keysym-set! (sdl2:event* sdl2:keysym* -> void))
(: keyboard-event-scancode-raw (sdl2:event* --> SDL_Scancode))
(: keyboard-event-scancode-raw-set! (sdl2:event* SDL_Scancode -> void))
(: keyboard-event-scancode (sdl2:event* --> symbol))
(: keyboard-event-scancode-set! (sdl2:event* enum -> void))
(: keyboard-event-sym-raw (sdl2:event* --> SDL_Keycode))
(: keyboard-event-sym-raw-set! (sdl2:event* SDL_Keycode -> void))
(: keyboard-event-sym (sdl2:event* --> symbol))
(: keyboard-event-sym-set! (sdl2:event* enum -> void))
(: keyboard-event-mod-raw (sdl2:event* --> Uint16))
(: keyboard-event-mod-raw-set! (sdl2:event* Uint16 -> void))
(: keyboard-event-mod (sdl2:event* --> (list-of symbol)))
(: keyboard-event-mod-set! (sdl2:event* enum-list -> void))

;;; sdl2-internals/accessors/events/mouse-button-event.scm

(: mouse-button-event? (any --> boolean))
(: mouse-button-event-window-id (sdl2:event* --> Uint32))
(: mouse-button-event-window-id-set! (sdl2:event* Uint32 -> void))
(: mouse-button-event-which (sdl2:event* --> Uint32))
(: mouse-button-event-which-set! (sdl2:event* Uint32 -> void))
(: mouse-button-event-button-raw (sdl2:event* --> Uint8))
(: mouse-button-event-button-raw-set! (sdl2:event* Uint8 -> void))
(: mouse-button-event-button (sdl2:event* --> symbol))
(: mouse-button-event-button-set! (sdl2:event* enum -> void))
(: mouse-button-event-state (sdl2:event* --> boolean))
(: mouse-button-event-state-set! (sdl2:event* boolean -> void))
(: mouse-button-event-x (sdl2:event* --> Sint32))
(: mouse-button-event-x-set! (sdl2:event* Sint32 -> void))
(: mouse-button-event-y (sdl2:event* --> Sint32))
(: mouse-button-event-y-set! (sdl2:event* Sint32 -> void))

;;; sdl2-internals/accessors/events/mouse-motion-event.scm

(: mouse-motion-event? (any --> boolean))
(: mouse-motion-event-window-id (sdl2:event* --> Uint32))
(: mouse-motion-event-window-id-set! (sdl2:event* Uint32 -> void))
(: mouse-motion-event-which (sdl2:event* --> Uint32))
(: mouse-motion-event-which-set! (sdl2:event* Uint32 -> void))
(: mouse-motion-event-state-raw (sdl2:event* --> Uint32))
(: mouse-motion-event-state-raw-set! (sdl2:event* Uint32 -> void))
(: mouse-motion-event-state (sdl2:event* --> (list-of symbol)))
(: mouse-motion-event-state-set! (sdl2:event* enum-list -> void))
(: mouse-motion-event-x (sdl2:event* --> Sint32))
(: mouse-motion-event-x-set! (sdl2:event* Sint32 -> void))
(: mouse-motion-event-y (sdl2:event* --> Sint32))
(: mouse-motion-event-y-set! (sdl2:event* Sint32 -> void))
(: mouse-motion-event-xrel (sdl2:event* --> Sint32))
(: mouse-motion-event-xrel-set! (sdl2:event* Sint32 -> void))
(: mouse-motion-event-yrel (sdl2:event* --> Sint32))
(: mouse-motion-event-yrel-set! (sdl2:event* Sint32 -> void))

;;; sdl2-internals/accessors/events/mouse-wheel-event.scm

(: mouse-wheel-event? (any --> boolean))
(: mouse-wheel-event-window-id (sdl2:event* --> Uint32))
(: mouse-wheel-event-window-id-set! (sdl2:event* Uint32 -> void))
(: mouse-wheel-event-which (sdl2:event* --> Uint32))
(: mouse-wheel-event-which-set! (sdl2:event* Uint32 -> void))
(: mouse-wheel-event-x (sdl2:event* --> Sint32))
(: mouse-wheel-event-x-set! (sdl2:event* Sint32 -> void))
(: mouse-wheel-event-y (sdl2:event* --> Sint32))
(: mouse-wheel-event-y-set! (sdl2:event* Sint32 -> void))
(: mouse-wheel-event-direction (sdl2:event* --> symbol))
(: mouse-wheel-event-direction-set! (sdl2:event* enum -> void))

;;; sdl2-internals/accessors/events/multi-gesture-event.scm

(: multi-gesture-event? (any --> boolean))
(: multi-gesture-event-touch-id (sdl2:event* --> SDL_TouchID))
(: multi-gesture-event-touch-id-set! (sdl2:event* SDL_TouchID -> void))
(: multi-gesture-event-dtheta (sdl2:event* --> float))
(: multi-gesture-event-dtheta-set! (sdl2:event* float -> void))
(: multi-gesture-event-ddist (sdl2:event* --> float))
(: multi-gesture-event-ddist-set! (sdl2:event* float -> void))
(: multi-gesture-event-x (sdl2:event* --> float))
(: multi-gesture-event-x-set! (sdl2:event* float -> void))
(: multi-gesture-event-y (sdl2:event* --> float))
(: multi-gesture-event-y-set! (sdl2:event* float -> void))
(: multi-gesture-event-num-fingers (sdl2:event* --> Uint16))
(: multi-gesture-event-num-fingers-set! (sdl2:event* Uint16 -> void))

;;; sdl2-internals/accessors/events/quit-event.scm

(: quit-event? (any --> boolean))

;;; sdl2-internals/accessors/events/sys-wm-event.scm

(: sys-wm-event? (any --> boolean))
(: sys-wm-event-msg-raw (sdl2:event* --> pointer))
(: sys-wm-event-msg-raw-set! (sdl2:event* pointer -> void))

;;; sdl2-internals/accessors/events/text-editing-event.scm

(: text-editing-event? (any --> boolean))
(: text-editing-event-window-id (sdl2:event* --> Uint32))
(: text-editing-event-window-id-set! (sdl2:event* Uint32 -> void))
(: text-editing-event-text (sdl2:event* --> string))
(: text-editing-event-text-set! (sdl2:event* string -> string))
(: text-editing-event-start (sdl2:event* --> Sint32))
(: text-editing-event-start-set! (sdl2:event* Sint32 -> void))
(: text-editing-event-length (sdl2:event* --> Sint32))
(: text-editing-event-length-set! (sdl2:event* Sint32 -> void))

;;; sdl2-internals/accessors/events/text-input-event.scm

(: text-input-event? (any --> boolean))
(: text-input-event-window-id (sdl2:event* --> Uint32))
(: text-input-event-window-id-set! (sdl2:event* Uint32 -> void))
(: text-input-event-text (sdl2:event* --> string))
(: text-input-event-text-set! (sdl2:event* string -> string))

;;; sdl2-internals/accessors/events/touch-finger-event.scm

(: touch-finger-event? (any --> boolean))
(: touch-finger-event-touch-id (sdl2:event* --> SDL_TouchID))
(: touch-finger-event-touch-id-set! (sdl2:event* SDL_TouchID -> void))
(: touch-finger-event-finger-id (sdl2:event* --> SDL_FingerID))
(: touch-finger-event-finger-id-set! (sdl2:event* SDL_FingerID -> void))
(: touch-finger-event-x (sdl2:event* --> float))
(: touch-finger-event-x-set! (sdl2:event* float -> void))
(: touch-finger-event-y (sdl2:event* --> float))
(: touch-finger-event-y-set! (sdl2:event* float -> void))
(: touch-finger-event-dx (sdl2:event* --> float))
(: touch-finger-event-dx-set! (sdl2:event* float -> void))
(: touch-finger-event-dy (sdl2:event* --> float))
(: touch-finger-event-dy-set! (sdl2:event* float -> void))
(: touch-finger-event-pressure (sdl2:event* --> float))
(: touch-finger-event-pressure-set! (sdl2:event* float -> void))

;;; sdl2-internals/accessors/events/user-event.scm

(: user-event? (any --> boolean))
(: user-event-window-id (sdl2:event* --> Uint32))
(: user-event-window-id-set! (sdl2:event* Uint32 -> void))
(: user-event-code (sdl2:event* --> Sint32))
(: user-event-code-set! (sdl2:event* Sint32 -> void))
(: user-event-data1-raw (sdl2:event* --> pointer))
(: user-event-data1-raw-set! (sdl2:event* pointer -> void))
(: user-event-data2-raw (sdl2:event* --> pointer))
(: user-event-data2-raw-set! (sdl2:event* pointer -> void))

;;; sdl2-internals/accessors/events/window-event.scm

(: window-event? (any --> boolean))
(: window-event-window-id (sdl2:event* --> Uint32))
(: window-event-window-id-set! (sdl2:event* Uint32 -> void))
(: window-event-event-raw (sdl2:event* --> Uint8))
(: window-event-event-raw-set! (sdl2:event* Uint8 -> void))
(: window-event-event (sdl2:event* --> symbol))
(: window-event-event-set! (sdl2:event* enum -> void))
(: window-event-data1 (sdl2:event* -> Sint32))
(: window-event-data1-set! (sdl2:event* Sint32 -> void))
(: window-event-data2 (sdl2:event* -> Sint32))
(: window-event-data2-set! (sdl2:event* Sint32 -> void))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; FINGER

;;; sdl2-internals/record-types/finger.scm

(: finger? (any --> boolean : sdl2:finger))

;;; sdl2-internals/accessors/finger.scm

(: finger-id (sdl2:finger* --> SDL_FingerID))
(: finger-x (sdl2:finger* --> float))
(: finger-y (sdl2:finger* --> float))
(: finger-pressure (sdl2:finger* --> float))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GAME CONTROLLER

;;; sdl2-internals/record-types/game-controller.scm

(: game-controller? (any --> boolean))
(: game-controller-button-bind? (any --> boolean))

;;; sdl2-internals/accessors/game-controller.scm

(: game-controller-button-bind-type
   (sdl2:game-controller-button-bind* --> symbol))
(: game-controller-button-bind-type-raw
   (sdl2:game-controller-button-bind* --> integer))
(: game-controller-button-bind-button
   (sdl2:game-controller-button-bind* --> integer))
(: game-controller-button-bind-axis
   (sdl2:game-controller-button-bind* --> integer))
(: game-controller-button-bind-hat
   (sdl2:game-controller-button-bind* --> integer))
(: game-controller-button-bind-hat-mask-raw
   (sdl2:game-controller-button-bind* --> integer))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GL-CONTEXT

;;; sdl2-internals/record-types/gl-context.scm

(: gl-context? (any --> boolean : sdl2:gl-context))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; JOYSTICK-GUID

;;; sdl2-internals/record-types/joystick-guid.scm

(: joystick-guid? (any --> boolean : sdl2:joystick-guid))
(: free-joystick-guid! (sdl2:joystick-guid -> void))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; JOYSTICK

;;; sdl2-internals/record-types/joystick.scm

(: joystick? (any --> boolean : sdl2:joystick))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; KEYSYM

;;; sdl2-internals/record-types/keysym.scm

(: keysym? (any --> boolean : sdl2:keysym))
(: free-keysym! (sdl2:keysym -> void))
(: alloc-keysym* (--> sdl2:keysym))
(: alloc-keysym (--> sdl2:keysym))

;;; sdl2-internals/accessors/keysym.scm

(: keysym-scancode-raw (sdl2:keysym* --> SDL_Scancode))
(: keysym-scancode-raw-set! (sdl2:keysym* SDL_Scancode -> void))
(: keysym-sym-raw (sdl2:keysym* --> SDL_Keycode))
(: keysym-sym-raw-set! (sdl2:keysym* SDL_Keycode -> void))
(: keysym-mod-raw (sdl2:keysym* --> Uint16))
(: keysym-mod-raw-set! (sdl2:keysym* Uint16 -> void))
(: keysym-scancode (sdl2:keysym* --> symbol))
(: keysym-scancode-set! (sdl2:keysym* enum -> void))
(: keysym-sym (sdl2:keysym* --> symbol))
(: keysym-sym-set! (sdl2:keysym* enum -> void))
(: keysym-mod (sdl2:keysym* --> (list-of symbol)))
(: keysym-mod-set! (sdl2:keysym* enum-list -> void))
(: make-keysym (#!optional enum enum enum-list -> sdl2:keysym))
(: make-keysym* (#!optional enum enum enum-list -> sdl2:keysym*))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PALETTE

;;; sdl2-internals/record-types/palette.scm

(: palette? (any --> boolean : sdl2:palette))
(: free-palette! (sdl2:palette -> void))

;;; sdl2-internals/accessors/palette.scm

(: palette-ncolors (sdl2:palette* --> fixnum))
(: make-palette (#!optional fixnum --> sdl2:palette))
(: palette-ncolours (sdl2:palette* --> fixnum))
(: make-palette* (#!optional fixnum --> sdl2:palette*))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PIXEL-FORMAT

;;; sdl2-internals/record-types/pixel-format.scm

(: pixel-format? (any --> boolean : sdl2:pixel-format))
(: free-pixel-format! (sdl2:pixel-format -> void))

;;; sdl2-internals/accessors/pixel-format.scm

(: pixel-format-format-raw (sdl2:pixel-format* --> SDL_PixelFormatEnum))
(: pixel-format-format (sdl2:pixel-format* --> symbol))
(: pixel-format-palette (sdl2:pixel-format* --> sdl2:palette))
(: pixel-format-palette-set! (sdl2:pixel-format* sdl2:palette* -> void))
(: pixel-format-bits-per-pixel (sdl2:pixel-format* --> Uint8))
(: pixel-format-bytes-per-pixel (sdl2:pixel-format* --> Uint8))
(: pixel-format-rmask (sdl2:pixel-format* --> Uint32))
(: pixel-format-gmask (sdl2:pixel-format* --> Uint32))
(: pixel-format-bmask (sdl2:pixel-format* --> Uint32))
(: pixel-format-amask (sdl2:pixel-format* --> Uint32))
(: make-pixel-format (#!optional enum -> sdl2:pixel-format))
(: make-pixel-format* (#!optional enum -> sdl2:pixel-format*))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; POINT

;;; sdl2-internals/record-types/point.scm

(: point? (any --> boolean : sdl2:point))
(: free-point! (sdl2:point -> void))
(: alloc-point* (--> sdl2:point))
(: alloc-point (--> sdl2:point))

;;; sdl2-internals/accessors/point.scm

(: point-x (sdl2:point* --> integer))
(: point-x-set! (sdl2:point* integer -> void))
(: point-y (sdl2:point* --> integer))
(: point-y-set! (sdl2:point* integer -> void))

;;; sdl2-internals/extras/point.scm

(: make-point (#!optional integer integer -> sdl2:point))
(: make-point* (#!optional integer integer -> sdl2:point))
(: point-set! (sdl2:point* #!optional integer integer -> sdl2:point*))
(: point->list (sdl2:point* -> (list integer integer)))
(: point->values (sdl2:point* -> integer integer))
(: point=? (sdl2:point* sdl2:point* -> boolean))
(: point-copy! (sdl2:point* sdl2:point* -> sdl2:point*))
(: point-copy (sdl2:point* -> sdl2:point))
(: point-scale! (sdl2:point* number #!optional sdl2:point* -> sdl2:point*))
(: point-scale (sdl2:point* number -> sdl2:point*))
(: point-unscale! (sdl2:point* number #!optional sdl2:point* -> sdl2:point*))
(: point-unscale (sdl2:point* number -> sdl2:point*))
(: point-move! (sdl2:point* integer integer #!optional sdl2:point* -> sdl2:point*))
(: point-move (sdl2:point* integer integer -> sdl2:point*))
(: point-add! (sdl2:point* sdl2:point* #!optional sdl2:point* -> sdl2:point*))
(: point-add (sdl2:point* sdl2:point* -> sdl2:point*))
(: point-sub! (sdl2:point* sdl2:point* #!optional sdl2:point* -> sdl2:point*))
(: point-sub (sdl2:point* sdl2:point* -> sdl2:point*))
(: point-lerp! (sdl2:point* sdl2:point* float #!optional sdl2:point* -> sdl2:point*))
(: point-lerp (sdl2:point* sdl2:point* float -> sdl2:point*))


;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RECT

;;; sdl2-internals/record-types/rect.scm

(: rect? (any --> boolean : sdl2:rect))
(: free-rect! (sdl2:rect -> void))
(: alloc-rect* (--> sdl2:rect))
(: alloc-rect (--> sdl2:rect))

;;; sdl2-internals/accessors/rect.scm

(: rect-x (sdl2:rect* --> integer))
(: rect-x-set! (sdl2:rect* integer -> void))
(: rect-y (sdl2:rect* --> integer))
(: rect-y-set! (sdl2:rect* integer -> void))
(: rect-w (sdl2:rect* --> integer))
(: rect-w-set! (sdl2:rect* integer -> void))
(: rect-h (sdl2:rect* --> integer))
(: rect-h-set! (sdl2:rect* integer -> void))

;;; sdl2-internals/extras/rect.scm

(: make-rect (#!optional integer integer integer integer -> sdl2:rect))
(: make-rect* (#!optional integer integer integer integer -> sdl2:rect))
(: rect-set! (sdl2:rect* #!optional integer integer integer integer -> sdl2:rect*))
(: rect->list (sdl2:rect* -> (list integer integer integer integer)))
(: rect->values (sdl2:rect* -> integer integer integer integer))
(: rect-copy! (sdl2:rect* sdl2:rect* -> sdl2:rect*))
(: rect-copy (sdl2:rect* -> sdl2:rect))
(: rect-scale! (sdl2:rect* number #!optional sdl2:rect* -> sdl2:rect*))
(: rect-scale (sdl2:rect* number -> sdl2:rect*))
(: rect-unscale! (sdl2:rect* number #!optional sdl2:rect* -> sdl2:rect*))
(: rect-unscale (sdl2:rect* number -> sdl2:rect*))
(: rect-move! (sdl2:rect* integer integer #!optional sdl2:rect* -> sdl2:rect*))
(: rect-move (sdl2:rect* integer integer -> sdl2:rect*))
(: rect-add-point! (sdl2:rect* sdl2:point* #!optional sdl2:rect* -> sdl2:rect*))
(: rect-add-point (sdl2:rect* sdl2:point* -> sdl2:rect*))
(: rect-sub-point! (sdl2:rect* sdl2:point* #!optional sdl2:rect* -> sdl2:rect*))
(: rect-sub-point (sdl2:rect* sdl2:point* -> sdl2:rect*))
(: rect-grow! (sdl2:rect* integer integer #!optional sdl2:rect* -> sdl2:rect*))
(: rect-grow (sdl2:rect* integer integer -> sdl2:rect*))
(: rect-grow! (sdl2:rect* integer integer #!optional sdl2:rect* -> sdl2:rect*))
(: rect-grow/center (sdl2:rect* integer integer -> sdl2:rect*))
(: rect-lerp! (sdl2:rect* sdl2:rect* float #!optional sdl2:rect* -> sdl2:rect*))
(: rect-lerp (sdl2:rect* sdl2:rect* float -> sdl2:rect*))
(: rect-lerp-xy! (sdl2:rect* sdl2:rect* float #!optional sdl2:rect* -> sdl2:rect*))
(: rect-lerp-xy (sdl2:rect* sdl2:rect* float -> sdl2:rect*))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RENDERER-INFO

;;; sdl2-internals/record-types/renderer-info.scm

(: renderer-info? (any --> boolean : sdl2:renderer-info))
(: free-renderer-info! (sdl2:renderer-info -> void))
(: alloc-renderer-info* (--> sdl2:renderer-info))
(: alloc-renderer-info (--> sdl2:renderer-info))

;;; sdl2-internals/accessors/renderer-info.scm

(: renderer-info-name (sdl2:renderer-info* --> string))
(: renderer-info-flags (sdl2:renderer-info* --> (list-of symbol)))
(: renderer-info-flags-raw (sdl2:renderer-info* --> Uint32))
(: renderer-info-num-texture-formats (sdl2:renderer-info* --> Uint32))
(: renderer-info-texture-formats (sdl2:renderer-info* --> (list-of symbol)))
(: renderer-info-texture-formats-raw (sdl2:renderer-info* --> (list-of Uint32)))
(: renderer-info-max-texture-width (sdl2:renderer-info* --> Sint32))
(: renderer-info-max-texture-height (sdl2:renderer-info* --> Sint32))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RENDERER

;;; sdl2-internals/record-types/renderer.scm

(: renderer? (any --> boolean : sdl2:renderer))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RWOPS

;;; sdl2-internals/record-types/rwops.scm

(: rwops? (any --> boolean : sdl2:rwops))

;;; sdl2-internals/accessors/rwops.scm

(: rwops-type-raw (sdl2:rwops* --> Uint32))
(: rwops-type (sdl2:rwops* --> symbol))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SURFACE

;;; sdl2-internals/record-types/surface.scm

(: surface? (any --> boolean : sdl2:surface))

(: surface-format (sdl2:surface* --> pointer))
(: surface-w (sdl2:surface* --> Sint32))
(: surface-h (sdl2:surface* --> Sint32))
(: surface-pitch (sdl2:surface* --> Sint32))
(: surface-pixels-raw (sdl2:surface* --> pointer))
(: surface-userdata-raw (sdl2:surface* --> pointer))
(: surface-userdata-raw-set! (sdl2:surface* pointer -> void))
(: surface-refcount (sdl2:surface* --> Sint32))
(: surface-refcount-set! (sdl2:surface* Sint32 -> void))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; TEXTURE

;;; sdl2-internals/record-types/texture.scm

(: texture? (any --> boolean : sdl2:texture))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VERSION

(: version-at-least? (fixnum fixnum fixnum --> boolean))
(: current-version (--> (list fixnum fixnum fixnum)))
(: compiled-version (--> (list fixnum fixnum fixnum)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOW

;;; sdl2-internals/record-types/window.scm

(: window? (any --> boolean : sdl2:window))
