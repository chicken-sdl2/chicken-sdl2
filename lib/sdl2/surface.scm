;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export free-surface!

        make-surface
        make-surface*
        create-rgb-surface*
        create-rgb-surface-from*
        create-rgb-surface-with-format* ;; SDL 2.0.5+
        create-rgb-surface-with-format-from* ;; SDL 2.0.5+

        convert-surface
        convert-surface*
        duplicate-surface
        duplicate-surface*

        load-bmp
        load-bmp*
        load-bmp-rw
        load-bmp-rw*
        save-bmp!
        save-bmp-rw!

        lock-surface!
        unlock-surface!
        must-lock?

        blit-surface!
        blit-scaled!

        fill-rect!
        fill-rects!

        surface-ref  surface-set!
        surface-ref-raw

        surface-clip-rect   surface-clip-rect-set!
        surface-color-key   surface-color-key-set!
        surface-color-key-raw
        surface-color-key?
        surface-colour-key  surface-colour-key-set!
        surface-colour-key-raw
        surface-colour-key?
        surface-alpha-mod   surface-alpha-mod-set!
        surface-blend-mode  surface-blend-mode-set!
        surface-blend-mode-raw
        surface-color-mod   surface-color-mod-set!
        surface-colour-mod  surface-colour-mod-set!
        surface-palette     surface-palette-set!
        surface-rle? ;; SDL 2.0.14+
        surface-rle-set!

        rotate-surface-90
        rotate-surface-90*
        flip-surface
        flip-surface*

        compose-custom-blend-mode

        get-yuv-conversion-mode ;; SDL 2.0.8+
        get-yuv-conversion-mode-raw ;; SDL 2.0.8+
        yuv-conversion-mode-set! ;; SDL 2.0.8+
        )


(: %map-color-for-surface
   ((or sdl2:color* integer) sdl2:surface symbol -> integer))
(define (%map-color-for-surface color surface fn-name)
  (cond
   ((color? color)
    (SDL_MapRGBA (surface-format surface)
                 (color-r color)
                 (color-g color)
                 (color-b color)
                 (color-a color)))
   ((and (integer? color)
         (not (negative? color)))
    color)
   (else
    (error fn-name
           "invalid color (expected sdl2:color or nonnegative integer)"
           color))))


(: %unmap-color-for-surface
   (integer sdl2:surface* -> sdl2:color))
(define (%unmap-color-for-surface pixel surface)
  (assert (and (integer? pixel) (not (negative? pixel))))
  (with-temp-mem ((r-out (%allocate-Uint8))
                  (g-out (%allocate-Uint8))
                  (b-out (%allocate-Uint8))
                  (a-out (%allocate-Uint8)))
    (SDL_GetRGBA pixel
                 (surface-format surface)
                 r-out g-out b-out a-out)
    (make-color (pointer-u8-ref r-out)
                (pointer-u8-ref g-out)
                (pointer-u8-ref b-out)
                (pointer-u8-ref a-out))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; FREE SURFACE

(: free-surface!
   (sdl2:surface* -> void))
(define (free-surface! surface)
  (SDL_FreeSurface surface)
  (%nullify-struct! surface)
  (void))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAKE / CREATE SURFACE

;;; Convenient way to create a new surface of a given size and pixel
;;; depth (bits per pixel) or pixel format. Format can be a symbol or
;;; integer constant > 64 (smaller values are treated as depth).
;;; The surface RGBA masks will be chosen automatically based on the
;;; requested depth. The surface will have a palette if depth <= 8.
;;; The surface will automatically be freed by the garbage collector.
;;; Returns #f if the surface could not be created (use get-error to
;;; find out why).
(: make-surface
   (integer integer (or integer symbol) -> sdl2:surface))
(define (make-surface width height depth-or-format)
  (let ((surface (make-surface* width height depth-or-format)))
    (set-finalizer! surface free-surface!)
    surface))


;;; Like make-surface, except the surface will NOT automatically
;;; be freed by the garbage collector. You must manually free the
;;; surface (e.g. using free-surface!) when you are done with it.
(: make-surface*
   (integer integer (or integer symbol) -> sdl2:surface))
(define (make-surface* width height depth-or-format)
  (assert (and (integer? width)  (positive? width)))
  (assert (and (integer? height) (positive? height)))
  (assert (or (symbol? depth-or-format)
              (and (integer? depth-or-format)
                   (positive? depth-or-format))))
  (cond-expand
   (libSDL-2.0.5+
    (if (or (symbol? depth-or-format)
            (> depth-or-format 64))
        (create-rgb-surface-with-format*
         0 width height 0 depth-or-format)
        (let-values (((depth rmask gmask bmask amask)
                      (%surface-settings depth-or-format)))
          (create-rgb-surface*
           0 width height depth rmask gmask bmask amask))))
   (else
    (let-values (((depth rmask gmask bmask amask)
                  (%surface-settings depth-or-format)))
      (create-rgb-surface*
       0 width height depth rmask gmask bmask amask)))))


(: %surface-settings
   ((or integer symbol) -> fixnum integer integer integer integer))
(define (%surface-settings depth-or-format)
  (cond
   ((or (symbol? depth-or-format) (> depth-or-format 64))
    (pixel-format-enum-to-masks
     (if (integer? depth-or-format)
         depth-or-format
         (symbol->pixel-format-enum depth-or-format))))
   ((= depth-or-format 32)
    ;; For depth 32, we need to give explicit masks because there
    ;; seems to be no way to specify a default alpha mask. For red,
    ;; green, and blue, 0 means to use the default, but for alpha, 0
    ;; means no alpha channel.
    (if (= SDL_BYTEORDER SDL_BIG_ENDIAN)
        (values 32
                #xff000000
                #x00ff0000
                #x0000ff00
                #x000000ff)
        (values 32
                #x000000ff
                #x0000ff00
                #x00ff0000
                #xff000000)))
   (else
    ;; For other depths, just use 0 to tell SDL to use defaults.
    (values depth-or-format 0 0 0 0))))


(define-syntax %assert-surface-mask!
  (syntax-rules ()
    ((%assert-surface-mask! value description)
     (assert (and (integer? value) (<= 0 value #xffffffff))
             (format "~A must be an integer in range [~S, ~S], but got: ~S"
                     description 0 #xffffffff value)))))


(: create-rgb-surface*
   (fixnum fixnum fixnum fixnum
           integer integer integer integer
           -> sdl2:surface*))
(define (create-rgb-surface* flags width height depth
                             rmask gmask bmask amask)
  (%assert-surface-mask! rmask "rmask")
  (%assert-surface-mask! gmask "gmask")
  (%assert-surface-mask! bmask "bmask")
  (%assert-surface-mask! amask "amask")
  (try-call (SDL_CreateRGBSurface
             flags width height depth
             (u32vector rmask gmask bmask amask))
            fail?: (%struct-fail surface?)))


(: create-rgb-surface-from*
   ((or pointer locative)
    fixnum fixnum fixnum fixnum
    integer integer integer integer
    -> sdl2:surface*))
(define (create-rgb-surface-from* pixels width height depth pitch
                                  rmask gmask bmask amask)
  (%assert-surface-mask! rmask "rmask")
  (%assert-surface-mask! gmask "gmask")
  (%assert-surface-mask! bmask "bmask")
  (%assert-surface-mask! amask "amask")
  (try-call (SDL_CreateRGBSurfaceFrom
             pixels width height depth pitch
             (u32vector rmask gmask bmask amask))
            fail?: (%struct-fail surface?)))


#+libSDL-2.0.5+
(: create-rgb-surface-with-format*
   (fixnum fixnum fixnum fixnum (or symbol integer)
           -> sdl2:surface*))
(define-versioned (create-rgb-surface-with-format*
                   flags width height depth format)
    libSDL-2.0.5+
  (let ((format-int (if (integer? format)
                        format
                        (symbol->pixel-format-enum format))))
    (try-call (SDL_CreateRGBSurfaceWithFormat
               flags width height depth (u32vector format-int))
              fail?: (%struct-fail surface?))))


#+libSDL-2.0.5+
(: create-rgb-surface-with-format-from*
   ((or pointer locative)
    fixnum fixnum fixnum fixnum (or symbol integer)
    -> sdl2:surface*))
(define-versioned (create-rgb-surface-with-format-from*
                   pixels width height depth pitch format)
    libSDL-2.0.5+
  (let ((format-int (if (integer? format)
                        format
                        (symbol->pixel-format-enum format))))
    (try-call (SDL_CreateRGBSurfaceWithFormatFrom
               pixels width height depth pitch (u32vector format-int))
              fail?: (%struct-fail surface?))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CONVERT / DUPLICATE SURFACE

(: convert-surface
   (sdl2:surface* (or sdl2:pixel-format* enum) -> sdl2:surface))
(define (convert-surface surface format)
  (%autofree-struct!
   (convert-surface* surface format)
   free-surface!))


(: convert-surface*
   (sdl2:surface* (or sdl2:pixel-format* enum) -> sdl2:surface))
(define (convert-surface* surface format)
  (let ((fmt (if (or (symbol? format) (integer? format))
                 (make-pixel-format format)
                 format)))
    (try-call (SDL_ConvertSurface surface fmt 0)
              fail?: (%struct-fail surface?))))


(: duplicate-surface
   (sdl2:surface* -> sdl2:surface))
(define (duplicate-surface surface)
  (%autofree-struct!
   (duplicate-surface* surface)
   free-surface!))


(: duplicate-surface*
   (sdl2:surface* -> sdl2:surface))
(define (duplicate-surface* surface)
  (cond-expand
   (libSDL-2.0.6+
    (try-call (SDL_DuplicateSurface surface)
              fail?: (%struct-fail surface?)))
   (else
    (try-call (SDL_ConvertSurface surface (surface-format surface) 0)
              fail?: (%struct-fail surface?)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LOAD / SAVE BMP

;;; Loads a BMP image file from the given file path (a string), and
;;; returns a new surface containing the image. The surface will
;;; automatically be freed by the garbage collector. Returns #f if the
;;; image could not be loaded (use get-error to find out why).
(: load-bmp
   (string -> sdl2:surface))
(define (load-bmp filepath)
  (let ((surface (load-bmp* filepath)))
    (set-finalizer! surface free-surface!)
    surface))

;;; Like load-bmp, except the surface will NOT automatically be
;;; freed by the garbage collector. You must manually free the surface
;;; (e.g. using free-surface!) when you are done with it.
(: load-bmp*
   (string -> sdl2:surface))
(define (load-bmp* filepath)
  (try-call (SDL_LoadBMP filepath)
            fail?: (%struct-fail surface?)))


;;; Loads a BMP image file from the given sdl2:rwops, and returns a new
;;; surface containing the image. If close? is #t, rwops will be
;;; closed after reading. The surface will automatically be freed by
;;; the garbage collector. Returns #f if the image could not be loaded
;;; (use get-error to find out why).
(: load-bmp-rw
   (sdl2:rwops #!optional boolean -> sdl2:surface))
(define (load-bmp-rw rwops #!optional close?)
  (let ((surface (load-bmp-rw* rwops close?)))
    (set-finalizer! surface free-surface!)
    surface))

;;; Like load-bmp-rw, except the surface will NOT automatically be
;;; freed by the garbage collector. You must manually free the surface
;;; (e.g. using free-surface!) when you are done with it.
(: load-bmp-rw*
   (sdl2:rwops #!optional boolean -> sdl2:surface))
(define (load-bmp-rw* rwops #!optional close?)
  (let ((surface (try-call (SDL_LoadBMP_RW rwops #f)
                           fail?: (%struct-fail surface?))))
    (when close? (rw-close! rwops))
    surface))


(: save-bmp!
   (sdl2:surface* string -> void))
(define (save-bmp! surface filepath)
  (try-call (SDL_SaveBMP surface filepath))
  (void))

(: save-bmp-rw!
   (sdl2:surface* sdl2:rwops* #!optional boolean -> void))
(define (save-bmp-rw! surface rwops #!optional close?)
  (try-call (SDL_SaveBMP_RW surface rwops #f))
  (when close? (rw-close! rwops))
  (void))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LOCK / UNLOCK

(: lock-surface!
   (sdl2:surface* -> void))
(define (lock-surface! surface)
  (try-call (SDL_LockSurface surface))
  (void))

(: unlock-surface!
   (sdl2:surface* -> void))
(define (unlock-surface! surface)
  (SDL_UnlockSurface surface))

(: must-lock?
   (sdl2:surface* -> boolean))
(define (must-lock? surface)
  (SDL_MUSTLOCK surface))

;;; Locks the surface (if needed), performs body, unlocks the surface
;;; (if needed), and returns value of the last body expression.
(define-syntax with-locked-surface
  (syntax-rules ()
    ((with-locked-surface surface body ...)
     (dynamic-wind
         (lambda ()
           (when (must-lock? surface)
             (lock-surface! surface)))
         (lambda ()
           body ...)
         (lambda ()
           (when (must-lock? surface)
             (unlock-surface! surface)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BLIT

(: blit-surface!
   (sdl2:surface*
    (or sdl2:rect* false)
    sdl2:surface*
    (or sdl2:rect* false)
    -> void))
(define (blit-surface! src src-rect dst dst-rect)
  (try-call (SDL_BlitSurface src src-rect dst dst-rect))
  (void))

(: blit-scaled!
   (sdl2:surface*
    (or sdl2:rect* false)
    sdl2:surface*
    (or sdl2:rect* false)
    -> void))
(define (blit-scaled! src src-rect dst dst-rect)
  (try-call (SDL_BlitScaled src src-rect dst dst-rect))
  (void))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; FILL RECT(S)

;;; Fill one area of the surface with a color. rect can be an
;;; sdl2:rect, or #f to fill the whole surface. color can be an
;;; sdl2:color or an integer (e.g. a mapped color from map-rgba, or
;;; a palette index).
(: fill-rect!
   (sdl2:surface*
    (or sdl2:rect* false)
    (or sdl2:color* integer)
    -> void))
(define (fill-rect! surface rect color)
  (let ((color-int (%map-color-for-surface
                    color surface 'fill-rect!)))
    (try-call (SDL_FillRect surface rect color-int))
    (void)))


;;; Fill multiple areas of the surface with a color. rects must be a
;;; list of sdl2:rects. color can be an sdl2:color or an integer (e.g. a
;;; mapped color from map-rgba, or a palette index).
(: fill-rects!
   (sdl2:surface*
    (or (list-of sdl2:rect*) (vector-of sdl2:rect*))
    (or sdl2:color* integer)
    -> void))
(define (fill-rects! surface rects color)
  (let ((color-int (%map-color-for-surface
                    color surface 'fill-rects!)))
    (receive (rect-array len) (%rects->array rects)
      (try-call (SDL_FillRects surface rect-array len color-int)
                on-fail: (free rect-array))
      (free rect-array)
      (void))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GET / SET PIXEL

(: %assert-surface-bounds
   (sdl2:surface* integer integer symbol -> *))
(define (%assert-surface-bounds surface x y fn-name)
  (assert-bounds x 0 (sub1 (surface-w surface))
                 "x coordinate out of bounds" fn-name)
  (assert-bounds y 0 (sub1 (surface-h surface))
                 "y coordinate out of bounds" fn-name))


;;; Set the pixel at the given x/y coordinates to the given sdl2:color
;;; or raw pixel value (like is returned by map-rgba). Throws an
;;; error if x or y is out of bounds for the surface size.
(: surface-set!
   (sdl2:surface*
    integer integer
    (or sdl2:color* integer)
    -> void))
(define (surface-set! surface x y color)
  (%assert-surface-bounds surface x y 'surface-set!)
  (let ((pixel (%map-color-for-surface color surface 'surface-set!)))
    (with-locked-surface
     surface
     (chickenSDL2_SurfaceSetPixel surface x y pixel))))

;;; Returns a sdl2:color instance for the pixel at the given x/y
;;; coordinates in the surface. Throws an error if x or y is out of
;;; bounds for the surface size.
(: surface-ref
   (sdl2:surface* integer integer -> sdl2:color))
(define (surface-ref surface x y)
  (%assert-surface-bounds surface x y 'surface-ref)
  (receive (r g b a) (get-rgba
                      (surface-ref-raw surface x y)
                      (surface-format surface))
    (make-color r g b a)))

(set! (setter surface-ref)
      surface-set!)

;;; Returns a raw pixel value (like is returned by map-rgba) for
;;; the pixel at the given x/y coordinates in the surface. Throws an
;;; error if x or y is out of bounds for the surface size.
(: surface-ref-raw
   (sdl2:surface* integer integer -> integer))
(define (surface-ref-raw surface x y)
  (%assert-surface-bounds surface x y 'surface-ref-raw)
  (with-locked-surface surface
                       (chickenSDL2_SurfaceGetPixel surface x y)))

(set! (setter surface-ref-raw)
      surface-set!)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SURFACE PROPERTIES

(: surface-clip-rect-set!
   (sdl2:surface*
    (or sdl2:rect* false)
    -> boolean))
(define (surface-clip-rect-set! surface rect)
  (SDL_SetClipRect surface rect))

(: surface-clip-rect
   (sdl2:surface* -> sdl2:rect*))
(define (surface-clip-rect surface)
  (let ((rect-out (alloc-rect)))
    (SDL_GetClipRect surface rect-out)
    rect-out))

(set! (setter surface-clip-rect)
      surface-clip-rect-set!)


;;; Set the surface's color key. color can be an sdl2:color, or an
;;; integer (e.g. a mapped color from map-rgba), or #f to disable
;;; the color key.
(: surface-color-key-set!
   (sdl2:surface* (or sdl2:color* integer false) -> void))
(define (surface-color-key-set! surface color)
  (let ((key? (not (not color)))
        (color-int
         (if color
             (%map-color-for-surface color surface
                                     'surface-color-key-set!)
             0)))
    (try-call (SDL_SetColorKey surface key? color-int))
    (void)))

;;; Returns the surface's color key as an sdl2:color, or #f if the
;;; surface has no color key. Signals exception if an error occurred.
(: surface-color-key
   (sdl2:surface* -> (or sdl2:color false)))
(define (surface-color-key surface)
  (%unmap-color-for-surface
   (surface-color-key-raw surface)
   surface))

(set! (setter surface-color-key)
      surface-color-key-set!)

;;; Returns the surface's color key as a non-negative integer (a
;;; mapped color), or #f if the surface has no color key. Signals
;;; exception if an error occurred.
(: surface-color-key-raw
   (sdl2:surface* -> (or integer false)))
(define (surface-color-key-raw surface)
  (with-temp-mem ((mapped-color-out (%allocate-Uint32)))
    (let ((ret-code (SDL_GetColorKey surface mapped-color-out)))
      (cond ((not (negative? ret-code))
             (pointer-u32-ref mapped-color-out))
            ((= -1 ret-code)
             #f)
            (else
             (free mapped-color-out)
             (abort (sdl-failure "SDL_GetColorKey" ret-code)))))))

(set! (setter surface-color-key-raw)
      surface-color-key-set!)

(: surface-color-key?
   (sdl2:surface* --> boolean))
(define (surface-color-key? surface)
  (cond-expand
   (libSDL-2.0.9+
    (SDL_HasColorKey surface))
   (else
    (with-temp-mem ((key-out (%allocate-Uint32)))
      (if (= 0 (SDL_GetColorKey surface key-out))
          #t
          (begin (SDL_ClearError)
                 #f))))))

(define surface-colour-key-set!  surface-color-key-set!)
(define surface-colour-key       surface-color-key)
(define surface-colour-key-raw   surface-color-key-raw)
(define surface-colour-key?      surface-color-key?)


(: surface-alpha-mod-set!
   (sdl2:surface* fixnum -> void))
(define (surface-alpha-mod-set! surface alpha)
  (try-call (SDL_SetSurfaceAlphaMod surface alpha))
  (void))

;;; Returns the surface's alpha mod as an integer in range [0, 255].
(: surface-alpha-mod
   (sdl2:surface* -> fixnum))
(define (surface-alpha-mod surface)
  (with-temp-mem ((alpha-out (%allocate-Uint8)))
    (try-call (SDL_GetSurfaceAlphaMod surface alpha-out)
              on-fail: (free alpha-out))
    (pointer-u8-ref alpha-out)))

(set! (setter surface-alpha-mod)
      surface-alpha-mod-set!)


(: surface-blend-mode
   (sdl2:surface* -> (or symbol integer)))
(define (surface-blend-mode surface)
  (blend-mode->symbol (surface-blend-mode-raw surface)
                      identity))


(: surface-blend-mode-raw
   (sdl2:surface* -> integer))
(define (surface-blend-mode-raw surface)
  (with-temp-mem ((mode-out (%allocate-Uint8)))
    (try-call (SDL_GetSurfaceBlendMode surface mode-out)
              on-fail: (free mode-out))
    (pointer-u8-ref mode-out)))


(: surface-blend-mode-set!
   (sdl2:surface* enum -> void))
(define (surface-blend-mode-set! surface mode)
  (define (bad-mode-err x)
    (error 'surface-blend-mode-set!
           "invalid surface blend mode" x))
  (let ((mode-int (if (integer? mode)
                      mode
                      (symbol->blend-mode mode bad-mode-err))))
    (try-call (SDL_SetSurfaceBlendMode surface mode-int))
    (void)))

(set! (setter surface-blend-mode)
      surface-blend-mode-set!)

(set! (setter surface-blend-mode-raw)
      surface-blend-mode-set!)


;;; Set the surface color mod. Accepts either an RGB(A) list of
;;; integers (0-255) or an sdl2:color (alpha will be ignored).
(: surface-color-mod-set!
   (sdl2:surface*
    (or sdl2:color
        (list fixnum fixnum fixnum)
        (list fixnum fixnum fixnum fixnum))
    -> void))
(define (surface-color-mod-set! surface rgb-or-color)
  (receive (r g b) (if (list? rgb-or-color)
                       (values (list-ref rgb-or-color 0)
                               (list-ref rgb-or-color 1)
                               (list-ref rgb-or-color 2))
                       (values (color-r rgb-or-color)
                               (color-g rgb-or-color)
                               (color-b rgb-or-color)))
    (try-call (SDL_SetSurfaceColorMod surface r g b))
    (void)))

;;; Returns the surface's color mod as 3 integers in the range [0,
;;; 255]. Signals an exception if an error occurred.
(: surface-color-mod
   (sdl2:surface* -> fixnum fixnum fixnum))
(define (surface-color-mod surface)
  (with-temp-mem ((r-out (%allocate-Uint8))
                  (g-out (%allocate-Uint8))
                  (b-out (%allocate-Uint8)))
    (try-call (SDL_GetSurfaceColorMod surface r-out g-out b-out)
              on-fail: (begin (free r-out)
                              (free g-out)
                              (free b-out)))
    (values (pointer-u8-ref r-out)
            (pointer-u8-ref g-out)
            (pointer-u8-ref b-out))))

(set! (setter surface-color-mod)
      surface-color-mod-set!)

(define surface-colour-mod-set! surface-color-mod-set!)
(define surface-colour-mod      surface-color-mod)


(: surface-palette-set!
   (sdl2:surface* sdl2:palette* -> void))
(define (surface-palette-set! surface palette)
  (try-call (SDL_SetSurfacePalette surface palette))
  (void))

(: surface-palette
   (sdl2:surface* -> sdl2:palette*))
(define (surface-palette surface)
  (pixel-format-palette (surface-format surface)))

(set! (setter surface-palette)
      surface-palette-set!)


#+libSDL-2.0.14+
(: surface-rle?
   (sdl2:surface* --> boolean))
(define-versioned (surface-rle? surface)
    libSDL-2.0.14+
  (SDL_HasSurfaceRLE surface))

(: surface-rle-set!
   (sdl2:surface* boolean -> void))
(define (surface-rle-set! surface rle?)
  (try-call (SDL_SetSurfaceRLE surface rle?))
  (void))

#+libSDL-2.0.14+
(set! (setter surface-rle?)
      surface-rle-set!)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SIMPLE TRANSFORMATIONS

;;; Return a copy of the given surface rotated by the given number of
;;; 90 degree clockwise turns. If the given surface has a palette, the
;;; new surface will share the same palette.
(: rotate-surface-90
   (sdl2:surface* fixnum -> sdl2:surface))
(define (rotate-surface-90 surface turns)
  (set-finalizer! (rotate-surface-90* surface turns)
                  free-surface!))

(: rotate-surface-90*
   (sdl2:surface* fixnum -> sdl2:surface))
(define (rotate-surface-90* surface turns)
  (try-call (chickenSDL2_RotateSurface90 surface turns)
            fail?: (%struct-fail surface?)))


;;; Return a copy of the given surface flipped on the X and/or Y axes.
;;; If the given surface has a palette, the new surface will share the
;;; same palette.
(: flip-surface
   (sdl2:surface* boolean boolean -> sdl2:surface))
(define (flip-surface surface flip-x? flip-y?)
  (set-finalizer! (flip-surface* surface flip-x? flip-y?)
                  free-surface!))

(: flip-surface*
   (sdl2:surface* boolean boolean -> sdl2:surface))
(define (flip-surface* surface flip-x? flip-y?)
  (try-call (chickenSDL2_FlipSurface surface flip-x? flip-y?)
            fail?: (%struct-fail surface?)))


#+libSDL-2.0.6+
(: compose-custom-blend-mode
   (enum enum enum enum enum enum -> integer))
(define-versioned (compose-custom-blend-mode
                   src-color-factor dst-color-factor color-operation
                   src-alpha-factor dst-alpha-factor alpha-operation)
    libSDL-2.0.6+

  (define (->factor x)
    (if (symbol? x) (symbol->blend-factor x) x))
  (define (->operation x)
    (if (symbol? x) (symbol->blend-operation x) x))

  (SDL_ComposeCustomBlendMode
   (->factor src-color-factor)
   (->factor dst-color-factor)
   (->operation color-operation)
   (->factor src-alpha-factor)
   (->factor dst-alpha-factor)
   (->operation alpha-operation)))


#+libSDL-2.0.8+
(: get-yuv-conversion-mode
   (--> symbol))
(define-versioned (get-yuv-conversion-mode)
    libSDL-2.0.8+
  (yuv-conversion-mode->symbol (SDL_GetYUVConversionMode)))

#+libSDL-2.0.8+
(: get-yuv-conversion-mode-raw
   (--> fixnum))
(define-versioned (get-yuv-conversion-mode-raw)
    libSDL-2.0.8+
  (SDL_GetYUVConversionMode))

#+libSDL-2.0.8+
(: yuv-conversion-mode-set!
   (enum -> void))
(define-versioned (yuv-conversion-mode-set! mode)
    libSDL-2.0.8+
  (let ((mode-int (if (integer? mode)
                      mode
                      (symbol->yuv-conversion-mode mode))))
    (SDL_SetYUVConversionMode mode-int)))

(set! (setter get-yuv-conversion-mode)
      yuv-conversion-mode-set!)
