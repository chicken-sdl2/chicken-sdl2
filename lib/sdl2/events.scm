;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export event-state  event-state-set!

        flush-event!
        flush-events!
        has-event?
        has-events?
        quit-requested?

        peek-events
        get-events!

        poll-event!
        pump-events!
        push-event!
        wait-event!
        wait-event-timeout!

        register-events!)


(: %event-type->int
   (enum #!optional symbol -> integer))
(define (%event-type->int type #!optional fn-name)
  (cond
   ((integer? type)
    type)
   ((symbol? type)
    (symbol->event-type
     type
     (lambda (t)
       (error fn-name "unrecognized event type" t))))
   (else
    (error fn-name "unrecognized event type" type))))


(: event-state-set!
   (enum boolean -> boolean))
(define (event-state-set! type state)
  (SDL_EventState
   (%event-type->int type 'event-state)
   (if state SDL_ENABLE SDL_DISABLE)))

(: event-state
   (enum -> boolean))
(define (event-state type)
  (SDL_EventState
   (%event-type->int type 'event-state)
   SDL_QUERY))

(set! (setter event-state)
      event-state-set!)


(: flush-event!
   (enum -> void))
(define (flush-event! type)
  (SDL_FlushEvent
   (%event-type->int type 'flush-event!)))

(: flush-events!
   (#!optional enum enum -> void))
(define (flush-events! #!optional
                       (min-type 'first)
                       (max-type 'last))
  (SDL_FlushEvents
   (%event-type->int min-type 'flush-events!)
   (%event-type->int max-type 'flush-events!)))

(: has-event?
   (enum -> boolean))
(define (has-event? type)
  (SDL_HasEvent
   (%event-type->int type 'has-event?)))

(: has-events?
   (#!optional enum enum -> boolean))
(define (has-events? #!optional
                     (min-type 'first)
                     (max-type 'last))
  (SDL_HasEvents
   (%event-type->int min-type 'has-events?)
   (%event-type->int max-type 'has-events?)))

(: quit-requested?
   (-> boolean))
(define (quit-requested?)
  (SDL_QuitRequested))


(: %peep-events
   (fixnum integer enum enum symbol -> (list-of sdl2:event)))
;;; Supports the "peek" and "get" actions of SDL_PeepEvents. The "add"
;;; action is not supported because it would almost certainly be less
;;; efficient than calling SDL_PushEvent repeatedly.
(define (%peep-events num action min-type max-type fn-name)
  (with-temp-mem ((event-array (%allocate-event-array num)))
    (let ((num-peeped
           (try-call
            (SDL_PeepEvents event-array num action
                            (%event-type->int min-type fn-name)
                            (%event-type->int max-type fn-name))
            on-fail: (free event-array))))
      (%event-array->list event-array num-peeped))))

(: peek-events
   (fixnum #!optional enum enum -> (list-of sdl2:event)))
(define (peek-events num #!optional
                     (min-type 'first)
                     (max-type 'last))
  (%peep-events num SDL_PEEKEVENT
                min-type max-type
                'peek-events))

(: get-events!
   (fixnum #!optional enum enum -> (list-of sdl2:event)))
(define (get-events! num #!optional
                     (min-type 'first)
                     (max-type 'last))
  (%peep-events num SDL_GETEVENT
                min-type max-type
                'get-events!))


(: poll-event!
   (#!optional sdl2:event* -> (or sdl2:event* false)))
(define (poll-event! #!optional event-out)
  (let ((ev (or event-out (alloc-event))))
    (if (SDL_PollEvent ev)
        ev
        #f)))

(: pump-events!
   (-> void))
(define (pump-events!)
  (SDL_PumpEvents))

(: push-event!
   (sdl2:event* -> boolean))
(define (push-event! event)
  (= 1 (try-call (SDL_PushEvent event))))


(: wait-event!
   (#!optional sdl2:event* (number -> *)
               -> sdl2:event*))
(define (wait-event! #!optional event-out delay-fn)
  ;; This does not actually use SDL_WaitEvent, because that function
  ;; blocks all SRFI-18 threads. See issue #22.
  ;; By default this uses delay!, but the user can provide a function
  ;; based on SRFI-18's thread-sleep! if they need multithreading.
  (let ((ev (or event-out (alloc-event)))
        (delay-fn (or delay-fn delay!)))
    (let loop ()
      (cond ((SDL_PollEvent ev)
             ev)
            (else
             (delay-fn 1)
             (loop))))))


(: wait-event-timeout!
   (fixnum #!optional sdl2:event* (number -> *)
           -> (or sdl2:event* false)))
(define (wait-event-timeout! timeout-ms #!optional event-out delay-fn)
  ;; NOTE: reverse argument order compared to SDL_WaitEventTimeout.
  ;; Also see note frome wait-event!.
  (let ((ev (or event-out (alloc-event)))
        (timeout-ticks (+ (SDL_GetTicks) timeout-ms))
        (delay-fn (or delay-fn delay!)))
    (let loop ()
      (cond ((SDL_PollEvent ev)
             ev)
            ((>= (SDL_GetTicks) timeout-ticks)
             #f)
            (else
             (delay-fn 1)
             (loop))))))


(: register-events!
   ((list-of symbol) -> (list-of (pair symbol integer))))
(define (register-events! event-symbols)
  (define (check-event-symbol event-symbol)
    (unless (symbol? event-symbol)
      (error 'register-events!
             "Invalid event type symbol"
             event-symbol))
    (when (%event-type-exists? event-symbol)
      (error 'register-events!
             "Event type symbol already used"
             event-symbol)))

  (define (register-event! event-symbol event-number)
    (%add-event-type! event-symbol event-number)
    (cons event-symbol event-number))

  ;; Check events BEFORE registration, so the registered numbers are
  ;; not wasted if any of the event symbols are invalid.
  (for-each check-event-symbol event-symbols)

  (if (null? event-symbols)
      ;; Nothing to register
      '()
      (let* ((numevents (length event-symbols))
             (response (SDL_RegisterEvents numevents)))
        (if (= response %SDL_RegisterEvents-failure-value)
            (abort (sdl-failure "SDL_RegisterEvents" response))
            (map register-event!
                 event-symbols
                 ;; A list of integers, length numevent, starting at
                 ;; response and counting up by 1.
                 (iota numevents response))))))
