;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export vulkan-load-library!
        vulkan-unload-library!
        vulkan-get-vk-get-instance-proc-addr
        vulkan-get-instance-extensions
        vulkan-get-drawable-size
        vulkan-create-surface)


#+libSDL-2.0.6+
(: vulkan-load-library!
   (#!optional (or string false) -> void))
(define-versioned (vulkan-load-library! #!optional path)
    libSDL-2.0.6+
  (try-call (SDL_Vulkan_LoadLibrary path))
  (void))


#+libSDL-2.0.6+
(: vulkan-unload-library!
   (-> void))
(define-versioned (vulkan-unload-library!)
    libSDL-2.0.6+
  (SDL_Vulkan_UnloadLibrary))


#+libSDL-2.0.6+
(: vulkan-get-vk-get-instance-proc-addr
   (--> pointer))
(define-versioned (vulkan-get-vk-get-instance-proc-addr)
    libSDL-2.0.6+
  (SDL_Vulkan_GetVkGetInstanceProcAddr))


#+libSDL-2.0.6+
(: vulkan-get-instance-extensions
   (sdl2:window* --> (list-of string)))
(define-versioned (vulkan-get-instance-extensions window)
    libSDL-2.0.6+
  (with-temp-mem ((success-out (%allocate-Uint8)))
    (let ((names (SDL_Vulkan_GetInstanceExtensions__Names
                  window success-out)))
      (if (zero? (pointer-u8-ref success-out))
          (begin (free success-out)
                 (abort (sdl-failure
                         "SDL_Vulkan_GetInstanceExtensions__Names"
                         #f)))
          names))))


#+libSDL-2.0.6+
(: vulkan-get-drawable-size
   (sdl2:window* --> fixnum fixnum))
(define-versioned (vulkan-get-drawable-size window)
    libSDL-2.0.6+
  (with-temp-mem ((w-out (%allocate-Sint32))
                  (h-out (%allocate-Sint32)))
    (SDL_Vulkan_GetDrawableSize window w-out h-out)
    (values (pointer-s32-ref w-out)
            (pointer-s32-ref h-out))))


#+libSDL-2.0.6+
(: vulkan-create-surface
   (sdl2:window pointer pointer -> void))
(define-versioned (vulkan-create-surface window instance surface-out)
    libSDL-2.0.6+
  (try-call (SDL_Vulkan_CreateSurface window instance surface-out)
            fail?: not))
