;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export capture-mouse!
        get-mouse-focus
        get-mouse-state
        get-mouse-state-raw
        get-global-mouse-state
        get-global-mouse-state-raw
        get-relative-mouse-state
        get-relative-mouse-state-raw
        get-relative-mouse-mode
        relative-mouse-mode-set!
        warp-mouse-in-window!
        warp-mouse-global!)


#+libSDL-2.0.4+
(: capture-mouse!
   (boolean -> void))
(define-versioned (capture-mouse! enabled)
    libSDL-2.0.4+
  (try-call (SDL_CaptureMouse enabled))
  (void))


(: get-mouse-focus
   (-> (or sdl2:window false)))
(define (get-mouse-focus)
  (let ((window (SDL_GetMouseFocus)))
    (if (struct-null? window)
        #f
        window)))


(: get-mouse-state
   (-> (list-of symbol) fixnum fixnum))
(define (get-mouse-state)
  (receive (btn-mask x y) (get-mouse-state-raw)
    (values (unpack-mouse-button-masks btn-mask)
            x
            y)))

(: get-mouse-state-raw
   (-> fixnum fixnum fixnum))
(define (get-mouse-state-raw)
  (with-temp-mem ((x-out (%allocate-Sint32))
                  (y-out (%allocate-Sint32)))
    (let ((btn-mask (SDL_GetMouseState x-out y-out)))
      (values btn-mask
              (pointer-s32-ref x-out)
              (pointer-s32-ref y-out)))))


#+libSDL-2.0.4+
(: get-global-mouse-state
   (-> (list-of symbol) fixnum fixnum))
(define-versioned (get-global-mouse-state)
    libSDL-2.0.4+
  (receive (btn-mask x y) (get-global-mouse-state-raw)
    (values (unpack-mouse-button-masks btn-mask)
            x
            y)))

#+libSDL-2.0.4+
(: get-global-mouse-state-raw
   (-> fixnum fixnum fixnum))
(define-versioned (get-global-mouse-state-raw)
    libSDL-2.0.4+
  (with-temp-mem ((x-out (%allocate-Sint32))
                  (y-out (%allocate-Sint32)))
    (let ((btn-mask (SDL_GetGlobalMouseState x-out y-out)))
      (values btn-mask
              (pointer-s32-ref x-out)
              (pointer-s32-ref y-out)))))


(: get-relative-mouse-state
   (-> (list-of symbol) fixnum fixnum))
(define (get-relative-mouse-state)
  (receive (btn-mask x y) (get-relative-mouse-state-raw)
    (values (unpack-mouse-button-masks btn-mask)
            x
            y)))

(: get-relative-mouse-state-raw
   (-> fixnum fixnum fixnum))
(define (get-relative-mouse-state-raw)
  (with-temp-mem ((x-out (%allocate-Sint32))
                  (y-out (%allocate-Sint32)))
    (let ((btn-mask (SDL_GetRelativeMouseState x-out y-out)))
      (values btn-mask
              (pointer-s32-ref x-out)
              (pointer-s32-ref y-out)))))


(: get-relative-mouse-mode
   (-> boolean))
(define (get-relative-mouse-mode)
  (SDL_GetRelativeMouseMode))

(: relative-mouse-mode-set!
   (boolean -> void))
(define (relative-mouse-mode-set! enabled)
  (try-call (SDL_SetRelativeMouseMode enabled))
  (void))

(set! (setter get-relative-mouse-mode)
      relative-mouse-mode-set!)


(: warp-mouse-in-window!
   (sdl2:window* fixnum fixnum -> void))
(define (warp-mouse-in-window! window x y)
  (SDL_WarpMouseInWindow window x y))

#+libSDL-2.0.4+
(: warp-mouse-global!
   (fixnum fixnum -> void))
(define-versioned (warp-mouse-global! x y)
    libSDL-2.0.4+
  (try-call (SDL_WarpMouseGlobal x y))
  (void))
