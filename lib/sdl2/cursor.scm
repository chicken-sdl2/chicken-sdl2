;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export create-cursor         create-cursor*
        create-color-cursor   create-color-cursor*
        create-colour-cursor  create-colour-cursor*
        create-system-cursor  create-system-cursor*
        free-cursor!
        get-cursor            cursor-set!
        get-default-cursor
        show-cursor?          show-cursor!)


(: create-cursor
   ((or u8vector blob locative pointer)
    (or u8vector blob locative pointer)
    fixnum fixnum fixnum fixnum -> sdl2:cursor))
(define (create-cursor data mask w h hot-x hot-y)
  (let ((cursor (create-cursor* data mask w h hot-x hot-y)))
    (set-finalizer! cursor free-cursor!)
    cursor))

(: create-cursor*
   ((or u8vector blob locative pointer)
    (or u8vector blob locative pointer)
    fixnum fixnum fixnum fixnum -> sdl2:cursor))
(define (create-cursor* data mask w h hot-x hot-y)
  (let ((data-ptr (if (or (locative? data) (pointer? data))
                      data
                      (make-locative data)))
        (mask-ptr (if (or (locative? mask) (pointer? mask))
                      mask
                      (make-locative mask))))
    (try-call (SDL_CreateCursor data-ptr mask-ptr w h hot-x hot-y)
              fail?: (%struct-fail cursor?))))


(: create-color-cursor
   (sdl2:surface* fixnum fixnum -> sdl2:cursor))
(define (create-color-cursor surface hot-x hot-y)
  (let ((cursor (create-color-cursor* surface hot-x hot-y)))
    (set-finalizer! cursor free-cursor!)
    cursor))

(: create-color-cursor*
   (sdl2:surface* fixnum fixnum -> sdl2:cursor))
(define (create-color-cursor* surface hot-x hot-y)
  (try-call (SDL_CreateColorCursor surface hot-x hot-y)
            fail?: (%struct-fail cursor?)))

(define create-colour-cursor create-color-cursor)
(define create-colour-cursor* create-color-cursor*)


(: create-system-cursor
   (enum -> sdl2:cursor))
(define (create-system-cursor id)
  (let ((cursor (create-system-cursor* id)))
    (set-finalizer! cursor free-cursor!)
    cursor))

(: create-system-cursor*
   (enum -> sdl2:cursor))
(define (create-system-cursor* id)
  (let ((id-int (if (integer? id)
                    id
                    (symbol->system-cursor id))))
    (try-call (SDL_CreateSystemCursor id-int)
              fail?: (%struct-fail cursor?))))


(: free-cursor!
   (sdl2:cursor* -> void))
(define (free-cursor! cursor)
  (SDL_FreeCursor cursor)
  (%nullify-struct! cursor)
  (void))


(: get-cursor
   (-> (or sdl2:cursor false)))
(define (get-cursor)
  (let ((cursor (SDL_GetCursor)))
    (if ((%struct-fail cursor?) cursor)
        #f
        cursor)))

(: cursor-set!
   (sdl2:cursor* -> void))
(define (cursor-set! cursor)
  (SDL_SetCursor cursor))

(set! (setter get-cursor)
      cursor-set!)


(: get-default-cursor
   (-> (or sdl2:cursor false)))
(define (get-default-cursor)
  (let ((cursor (SDL_GetDefaultCursor)))
    (if ((%struct-fail cursor?) cursor)
        #f
        cursor)))


(: show-cursor?
   (-> boolean))
(define (show-cursor?)
  (case (try-call (SDL_ShowCursor SDL_QUERY))
    ((1) #t)
    (else #f)))

(: show-cursor!
   (boolean -> void))
(define (show-cursor! visible)
  (try-call (SDL_ShowCursor (if visible SDL_ENABLE SDL_DISABLE)))
  (void))
