;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export create-renderer!
        create-software-renderer!
        create-window-and-renderer!
        destroy-renderer!
        get-renderer
        get-renderer-info  get-renderer-info*
        renderer-output-size
        num-render-drivers
        render-driver-info
        render-present!

        render-clip-rect     render-clip-rect-set!
        render-clip-enabled? ;; SDL >= 2.0.4
        render-integer-scale?  render-integer-scale-set!
        render-logical-size  render-logical-size-set!
        render-scale         render-scale-set!
        render-viewport      render-viewport-set!

        render-copy!
        render-copy-ex!
        render-target-supported?
        render-target
        render-target-set!
        render-read-pixels-raw)


(: create-renderer!
   (sdl2:window* #!optional fixnum enum-list
    -> sdl2:renderer))
(define (create-renderer! window #!optional (index -1) (flags '()))
  (try-call (SDL_CreateRenderer window index
                                (pack-renderer-flags flags))
            fail?: (%struct-fail renderer?)))


(: create-software-renderer!
   (sdl2:surface* -> sdl2:renderer))
(define (create-software-renderer! surface)
  (try-call (SDL_CreateSoftwareRenderer surface)
            fail?: (%struct-fail renderer?)))


(: create-window-and-renderer!
   (integer integer #!optional enum-list
    -> sdl2:window sdl2:renderer))
(define (create-window-and-renderer! width height #!optional (window-flags '()))
  ;; This is a bit tricky because SDL_CreateWindowAndRenderer uses
  ;; pointer-pointers (SDL_Window** and SDL_Renderer**).
  (let ((window**   (make-pointer-vector 1))
        (renderer** (make-pointer-vector 1)))
    (try-call (SDL_CreateWindowAndRenderer
               width height
               (pack-window-flags window-flags)
               window** renderer**))
    (values (wrap-window   (pointer-vector-ref window**   0))
            (wrap-renderer (pointer-vector-ref renderer** 0)))))


(: destroy-renderer!
   (sdl2:renderer* -> void))
(define (destroy-renderer! renderer)
  (SDL_DestroyRenderer renderer))


(: get-renderer
   (sdl2:window* -> sdl2:renderer))
(define (get-renderer window)
  (try-call (SDL_GetRenderer window)
            fail?: (%struct-fail renderer?)))


(: get-renderer-info
   (sdl2:renderer* -> sdl2:renderer-info))
(define (get-renderer-info renderer)
  (set-finalizer! (get-renderer-info* renderer)
                  free-renderer-info!))

(: get-renderer-info*
   (sdl2:renderer* -> sdl2:renderer-info))
(define (get-renderer-info* renderer)
  (let ((info (alloc-renderer-info)))
    (try-call (SDL_GetRendererInfo renderer info)
              on-fail: (free-renderer-info! info))
    info))


(: renderer-output-size
   (sdl2:renderer* -> integer integer))
(define (renderer-output-size renderer)
  (with-temp-mem ((w-out (%allocate-Sint32))
                  (h-out (%allocate-Sint32)))
    (try-call (SDL_GetRendererOutputSize renderer w-out h-out)
              on-fail: (begin (free w-out)
                              (free h-out)))
    (values (pointer-s32-ref w-out)
            (pointer-s32-ref h-out))))


(: num-render-drivers
   (-> fixnum))
(define (num-render-drivers)
  (let ((num (SDL_GetNumRenderDrivers)))
    (if (negative? num)
        (abort (sdl-failure "SDL_GetNumRenderDrivers" num))
        num)))


(: render-driver-info
   (fixnum -> sdl2:renderer-info))
(define (render-driver-info index)
  (let ((info (alloc-renderer-info)))
    (try-call (SDL_GetRenderDriverInfo index info)
              on-fail: (free-renderer-info! info))
    info))


(: render-present!
   (sdl2:renderer* -> void))
(define (render-present! renderer)
  (SDL_RenderPresent renderer))


(: render-clip-rect
   (sdl2:renderer* -> sdl2:rect))
(define (render-clip-rect renderer)
  (let ((rect-out (alloc-rect)))
    (SDL_RenderGetClipRect renderer rect-out)
    rect-out))

(: render-clip-rect-set!
   (sdl2:renderer* (or sdl2:rect* false) -> void))
(define (render-clip-rect-set! renderer rect)
  (try-call (SDL_RenderSetClipRect renderer rect))
  (void))

(set! (setter render-clip-rect) render-clip-rect-set!)


#+libSDL-2.0.4+
(: render-clip-enabled?
   (sdl2:renderer* -> boolean))
(define-versioned (render-clip-enabled? renderer)
    libSDL-2.0.4+
  (SDL_RenderIsClipEnabled renderer))


#+libSDL-2.0.5+
(: render-integer-scale?
   (sdl2:renderer* -> boolean))
(define-versioned (render-integer-scale? renderer)
    libSDL-2.0.5+
  (SDL_RenderGetIntegerScale renderer))

#+libSDL-2.0.5+
(: render-integer-scale-set!
   (sdl2:renderer* boolean -> void))
(define-versioned (render-integer-scale-set! renderer enabled)
    libSDL-2.0.5+
  (try-call (SDL_RenderSetIntegerScale renderer enabled))
  (void))

(set! (setter render-integer-scale?) render-integer-scale-set!)


(: render-logical-size
   (sdl2:renderer* -> integer integer))
(define (render-logical-size renderer)
  (with-temp-mem ((w-out (%allocate-Sint32))
                  (h-out (%allocate-Sint32)))
    (SDL_RenderGetLogicalSize renderer w-out h-out)
    (values (pointer-s32-ref w-out)
            (pointer-s32-ref h-out))))

(: render-logical-size-set!
   (sdl2:renderer* (list integer integer) -> void))
(define (render-logical-size-set! renderer size)
  (try-call (SDL_RenderSetLogicalSize
             renderer (car size) (cadr size)))
  (void))

(set! (setter render-logical-size) render-logical-size-set!)


(: render-scale
   (sdl2:renderer* -> float float))
(define (render-scale renderer)
  (with-temp-mem ((scale-x-out (%allocate-float))
                  (scale-y-out (%allocate-float)))
    (SDL_RenderGetScale renderer scale-x-out scale-y-out)
    (values (pointer-f32-ref scale-x-out)
            (pointer-f32-ref scale-y-out))))

(: render-scale-set!
   (sdl2:renderer* (list float float) -> void))
(define (render-scale-set! renderer scale)
  (try-call (SDL_RenderSetScale renderer (car scale) (cadr scale)))
  (void))

(set! (setter render-scale) render-scale-set!)


(: render-viewport
   (sdl2:renderer* -> sdl2:rect))
(define (render-viewport renderer)
  (let ((rect-out (alloc-rect)))
    (SDL_RenderGetViewport renderer rect-out)
    rect-out))

(: render-viewport-set!
   (sdl2:renderer* (or sdl2:rect* false) -> void))
(define (render-viewport-set! renderer rect)
  (try-call (SDL_RenderSetViewport renderer rect))
  (void))

(set! (setter render-viewport) render-viewport-set!)


(: render-copy!
   (sdl2:renderer*
    sdl2:texture*
    #!optional
    (or sdl2:rect false)
    (or sdl2:rect false)
    -> void))
(define (render-copy! renderer texture
                      #!optional srcrect dstrect)
  (try-call (SDL_RenderCopy renderer texture srcrect dstrect))
  (void))

(: render-copy-ex!
   (sdl2:renderer* sdl2:texture*
    #!optional
    (or sdl2:rect* false)
    (or sdl2:rect* false)
    float
    (or sdl2:point* false)
    enum-list
    -> void))
(define (render-copy-ex! renderer texture
                         #!optional
                         srcrect dstrect
                         (angle 0.0)
                         center
                         (flip '()))
  (try-call (SDL_RenderCopyEx
             renderer texture
             srcrect dstrect
             angle center
             (if (integer? flip)
                 flip
                 (pack-renderer-flip flip))))
  (void))


(: render-target-supported?
   (sdl2:renderer* -> boolean))
(define (render-target-supported? renderer)
  (SDL_RenderTargetSupported renderer))


(: render-target
   (sdl2:renderer* -> (or sdl2:texture false)))
(define (render-target renderer)
  (let ((texture (SDL_GetRenderTarget renderer)))
    (if (struct-null? texture)
        #f
        texture)))

(: render-target-set!
   (sdl2:renderer* sdl2:texture* -> void))
(define (render-target-set! renderer texture)
  (try-call (SDL_SetRenderTarget renderer texture))
  (void))

(set! (setter render-target) render-target-set!)


(: render-read-pixels-raw
   (sdl2:renderer*
    (or sdl2:rect* false)
    enum
    (or pointer locative)
    fixnum
    -> void))
(define (render-read-pixels-raw renderer rect format pixels-out pitch)
  (let ((format-int (if (integer? format)
                        format
                        (symbol->pixel-format-enum format))))
    (try-call (SDL_RenderReadPixels
               renderer rect format-int pixels-out pitch))
    (void)))
