;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export num-joysticks
        joystick-open!
        joystick-close!
        joystick-from-instance-id

        joystick-update!
        joystick-event-state  joystick-event-state-set!
        joystick-attached?
        joystick-current-power-level

        joystick-num-axes
        joystick-num-balls
        joystick-num-buttons
        joystick-num-hats

        joystick-get-axis
        joystick-get-ball
        joystick-get-button
        joystick-get-hat
        joystick-get-hat-raw

        joystick-instance-id
        joystick-name
        joystick-name-for-index

        joystick-get-device-guid
        joystick-get-guid
        joystick-get-guid-from-string
        joystick-get-guid-string

        joystick-get-device-vendor
        joystick-get-device-product
        joystick-get-device-product-version
        joystick-get-device-type
        joystick-get-device-type-raw
        joystick-get-device-instance-id
        joystick-get-device-player-index

        joystick-get-vendor
        joystick-get-product
        joystick-get-product-version
        joystick-get-type
        joystick-get-type-raw
        joystick-get-player-index

        joystick-get-axis-initial-state

        lock-joysticks!
        unlock-joysticks!

        joystick-rumble!)


(: num-joysticks
   (-> fixnum))
(define (num-joysticks)
  (try-call (SDL_NumJoysticks)))


(: joystick-open!
   (fixnum -> sdl2:joystick))
(define (joystick-open! device-index)
  (try-call (SDL_JoystickOpen device-index)
            fail?: (%struct-fail joystick?)))

(: joystick-close!
   (sdl2:joystick* -> void))
(define (joystick-close! joystick)
  (SDL_JoystickClose joystick))


#+libSDL-2.0.4+
(: joystick-from-instance-id
   (fixnum -> sdl2:joystick*))
(define-versioned (joystick-from-instance-id joy-id)
    libSDL-2.0.4+
  (try-call (SDL_JoystickFromInstanceID joy-id)
            fail?: (%struct-fail joystick?)))


(: joystick-update!
   (-> void))
(define (joystick-update!)
  (SDL_JoystickUpdate))


(: joystick-event-state-set!
   (boolean -> boolean))
(define (joystick-event-state-set! state)
  (let ((state-int (case state
                     ((#t) SDL_ENABLE)
                     ((#f) SDL_IGNORE)
                     (else (error 'joystick-event-state-set!
                                  "invalid state" state)))))
    (= (try-call (SDL_JoystickEventState state-int))
       SDL_ENABLE)))

(: joystick-event-state
   (-> boolean))
(define (joystick-event-state)
  (= (try-call (SDL_JoystickEventState SDL_QUERY))
     SDL_ENABLE))

(set! (setter joystick-event-state)
      joystick-event-state-set!)


(: joystick-attached?
   (sdl2:joystick* -> boolean))
(define (joystick-attached? joystick)
  (SDL_JoystickGetAttached joystick))


#+libSDL-2.0.4+
(: joystick-current-power-level
   (sdl2:joystick* -> symbol))
(define-versioned (joystick-current-power-level joystick)
    libSDL-2.0.4+
  (joystick-power-level->symbol
   (SDL_JoystickCurrentPowerLevel joystick)))


(: joystick-num-axes
   (sdl2:joystick* -> fixnum))
(define (joystick-num-axes joystick)
  (try-call (SDL_JoystickNumAxes joystick)))

(: joystick-num-balls
   (sdl2:joystick* -> fixnum))
(define (joystick-num-balls joystick)
  (try-call (SDL_JoystickNumBalls joystick)))

(: joystick-num-buttons
   (sdl2:joystick* -> fixnum))
(define (joystick-num-buttons joystick)
  (try-call (SDL_JoystickNumButtons joystick)))

(: joystick-num-hats
   (sdl2:joystick* -> fixnum))
(define (joystick-num-hats joystick)
  (try-call (SDL_JoystickNumHats joystick)))


(: joystick-get-axis
   (sdl2:joystick* fixnum -> fixnum))
(define (joystick-get-axis joystick axis)
  (assert-bounds axis 0 (sub1 (joystick-num-axes joystick))
                 "axis number out of bounds" 'joystick-get-axis)
  (SDL_JoystickGetAxis joystick axis))

(: joystick-get-ball
   (sdl2:joystick* fixnum -> fixnum fixnum))
(define (joystick-get-ball joystick ball)
  (assert-bounds ball 0 (sub1 (joystick-num-balls joystick))
                 "ball number out of bounds" 'joystick-get-ball)
  (with-temp-mem ((dx-out (%allocate-Sint32))
                  (dy-out (%allocate-Sint32)))
    (try-call (SDL_JoystickGetBall joystick ball dx-out dy-out)
              on-fail: (begin (free dx-out)
                              (free dy-out)))
    (values (pointer-s32-ref dx-out)
            (pointer-s32-ref dy-out))))

(: joystick-get-button
   (sdl2:joystick* fixnum -> boolean))
(define (joystick-get-button joystick button)
  (assert-bounds button 0 (sub1 (joystick-num-buttons joystick))
                 "button number out of bounds" 'joystick-get-button)
  (SDL_JoystickGetButton joystick button))

(: joystick-get-hat
   (sdl2:joystick* fixnum -> symbol))
(define (joystick-get-hat joystick hat)
  (assert-bounds hat 0 (sub1 (joystick-num-hats joystick))
                 "hat number out of bounds" 'joystick-get-hat)
  (joystick-hat-position->symbol
   (SDL_JoystickGetHat joystick hat)))

(: joystick-get-hat-raw
   (sdl2:joystick* fixnum -> fixnum))
(define (joystick-get-hat-raw joystick hat)
  (assert-bounds hat 0 (sub1 (joystick-num-hats joystick))
                 "hat number out of bounds" 'joystick-get-hat-raw)
  (SDL_JoystickGetHat joystick hat))


(: joystick-instance-id
   (sdl2:joystick* -> fixnum))
(define (joystick-instance-id joystick)
  (try-call (SDL_JoystickInstanceID joystick)))

(: joystick-name
   (sdl2:joystick* -> string))
(define (joystick-name joystick)
  (SDL_JoystickName joystick))

(: joystick-name-for-index
   (fixnum -> string))
(define (joystick-name-for-index device-index)
  (SDL_JoystickNameForIndex device-index))


(: joystick-get-device-guid
   (fixnum -> sdl2:joystick-guid))
(define (joystick-get-device-guid device-index)
  (%autofree-struct!
   (SDL_JoystickGetDeviceGUID device-index)
   free-joystick-guid!))

(: joystick-get-guid
   (sdl2:joystick* -> sdl2:joystick-guid))
(define (joystick-get-guid joystick)
  (%autofree-struct!
   (SDL_JoystickGetGUID joystick)
   free-joystick-guid!))

(: joystick-get-guid-from-string
   (string -> sdl2:joystick-guid))
(define (joystick-get-guid-from-string guid-string)
  (%autofree-struct!
   (SDL_JoystickGetGUIDFromString guid-string)
   free-joystick-guid!))

(: joystick-get-guid-string
   (sdl2:joystick-guid* -> string))
(define (joystick-get-guid-string guid)
  (%joystick-get-guid-string guid))


#+libSDL-2.0.6+
(: joystick-get-device-vendor
   (integer --> integer))
(define-versioned (joystick-get-device-vendor device-index)
    libSDL-2.0.6+
  (SDL_JoystickGetDeviceVendor device-index))

#+libSDL-2.0.6+
(: joystick-get-device-product
   (integer --> integer))
(define-versioned (joystick-get-device-product device-index)
    libSDL-2.0.6+
  (SDL_JoystickGetDeviceProduct device-index))

#+libSDL-2.0.6+
(: joystick-get-device-product-version
   (integer --> integer))
(define-versioned (joystick-get-device-product-version device-index)
    libSDL-2.0.6+
  (SDL_JoystickGetDeviceProductVersion device-index))

#+libSDL-2.0.6+
(: joystick-get-device-type
   (integer --> symbol))
(define-versioned (joystick-get-device-type device-index)
    libSDL-2.0.6+
  (joystick-type->symbol
   (SDL_JoystickGetDeviceType device-index)))

#+libSDL-2.0.6+
(: joystick-get-device-type-raw
   (integer --> fixnum))
(define-versioned (joystick-get-device-type-raw device-index)
    libSDL-2.0.6+
  (SDL_JoystickGetDeviceType device-index))

#+libSDL-2.0.6+
(: joystick-get-device-instance-id
   (integer --> SDL_JoystickID))
(define-versioned (joystick-get-device-instance-id device-index)
    libSDL-2.0.6+
  (SDL_JoystickGetDeviceInstanceID device-index))

#+libSDL-2.0.9+
(: joystick-get-device-player-index
   (integer --> integer))
(define-versioned (joystick-get-device-player-index device-index)
    libSDL-2.0.9+
  (SDL_JoystickGetDevicePlayerIndex device-index))


#+libSDL-2.0.6+
(: joystick-get-vendor
   (sdl2:joystick* --> integer))
(define-versioned (joystick-get-vendor joystick)
    libSDL-2.0.6+
  (SDL_JoystickGetVendor joystick))

#+libSDL-2.0.6+
(: joystick-get-product
   (sdl2:joystick* --> integer))
(define-versioned (joystick-get-product joystick)
    libSDL-2.0.6+
  (SDL_JoystickGetProduct joystick))

#+libSDL-2.0.6+
(: joystick-get-product-version
   (sdl2:joystick* --> integer))
(define-versioned (joystick-get-product-version joystick)
    libSDL-2.0.6+
  (SDL_JoystickGetProductVersion joystick))

#+libSDL-2.0.6+
(: joystick-get-type
   (sdl2:joystick* --> symbol))
(define-versioned (joystick-get-type joystick)
    libSDL-2.0.6+
  (joystick-type->symbol
   (SDL_JoystickGetType joystick)))

#+libSDL-2.0.6+
(: joystick-get-type-raw
   (sdl2:joystick* --> integer))
(define-versioned (joystick-get-type-raw joystick)
    libSDL-2.0.6+
  (SDL_JoystickGetType joystick))

#+libSDL-2.0.9+
(: joystick-get-player-index
   (sdl2:joystick* --> integer))
(define-versioned (joystick-get-player-index joystick)
    libSDL-2.0.9+
  (SDL_JoystickGetPlayerIndex joystick))


#+libSDL-2.0.6+
(: joystick-get-axis-initial-state
   (sdl2:joystick* enum --> boolean integer))
(define-versioned (joystick-get-axis-initial-state joystick axis)
    libSDL-2.0.6+
  (with-temp-mem ((state-out (%allocate-Sint16)))
    (let ((result (SDL_JoystickGetAxisInitialState
                   joystick axis state-out)))
      (values (pointer-s16-ref state-out)
              result))))


#+libSDL-2.0.7+
(: lock-joysticks!
   (-> void))
(define-versioned (lock-joysticks!)
    libSDL-2.0.7+
  (SDL_LockJoysticks))

#+libSDL-2.0.7+
(: unlock-joysticks!
   (-> void))
(define-versioned (unlock-joysticks!)
    libSDL-2.0.7+
  (SDL_UnlockJoysticks))


#+libSDL-2.0.9+
(: joystick-rumble!
   (sdl2:joystick* integer integer integer -> boolean))
(define-versioned (joystick-rumble! joy low-freq hi-freq duration-ms)
    libSDL-2.0.9+
  (= 0 (SDL_JoystickRumble joy low-freq hi-freq duration-ms)))
