;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export game-controller?
        wrap-game-controller
        unwrap-game-controller
        %game-controller-pointer
        %game-controller-pointer-set!

        game-controller-button-bind?
        wrap-game-controller-button-bind
        unwrap-game-controller-button-bind
        %game-controller-button-bind-pointer
        %game-controller-button-bind-pointer-set!)


;;; NOTE: Public type declarations are in shared/reexport-types.scm


(define-struct-record-type
  sdl2:game-controller "SDL_GameController"
  pred:    game-controller?
  wrap:    wrap-game-controller
  unwrap:  unwrap-game-controller
  (pointer %game-controller-pointer
           %game-controller-pointer-set!))

(define-struct-record-printer sdl2:game-controller
  %game-controller-pointer
  show-address: #t)


(define-struct-record-type
  sdl2:game-controller-button-bind "SDL_GameControllerButtonBind"
  pred:    game-controller-button-bind?
  wrap:    wrap-game-controller-button-bind
  unwrap:  unwrap-game-controller-button-bind
  (pointer %game-controller-button-bind-pointer
           %game-controller-button-bind-pointer-set!))

(define-struct-memory-helpers
  "SDL_GameControllerButtonBind"
  using: (wrap-game-controller-button-bind
          game-controller-button-bind?
          %game-controller-button-bind-pointer
          %game-controller-button-bind-pointer-set!)
  define: (free-game-controller-button-bind!
           %alloc-game-controller-button-bind*
           %alloc-game-controller-button-bind))

(define-struct-record-printer sdl2:game-controller-button-bind
  %game-controller-button-bind-pointer
  show-address: #t
  (type game-controller-button-bind-type))
