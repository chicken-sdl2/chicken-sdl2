;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export SDL_Init
        SDL_InitSubSystem
        SDL_Quit
        SDL_QuitSubSystem
        SDL_WasInit
        SDL_SetMainReady

        SDL_ClearError
        SDL_GetError
        SDL_SetError

        SDL_GetPlatform
        SDL_GetPowerInfo

        SDL_DisableScreenSaver
        SDL_EnableScreenSaver
        SDL_IsScreenSaverEnabled

        SDL_ShowSimpleMessageBox

        SDL_HasClipboardText
        SDL_GetClipboardText
        SDL_SetClipboardText

        current-version
        compiled-version
        SDL_VERSION_ATLEAST
        version-at-least?)

#+libSDL-2.0.1+
(export SDL_GetBasePath
        SDL_GetPrefPath)

#+libSDL-2.0.9+
(export SDL_IsTablet)

#+libSDL-2.0.14+
(export SDL_OpenURL)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INITIALIZATION AND SHUTDOWN

(define-function-binding SDL_Init
  return: (Sint32 zero-if-success)
  args: ((Uint32 flags)))

(define-function-binding SDL_InitSubSystem
  return: (Sint32 zero-if-success)
  args: ((Uint32 flags)))

(define-function-binding SDL_Quit)

(define-function-binding SDL_QuitSubSystem
  args: ((Uint32 flags)))

(define-function-binding SDL_WasInit
  return: (Uint32 initialized-subsystems-mask)
  args: ((Uint32 flags)))

(define-function-binding SDL_SetMainReady)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ERROR HANDLING

(define-function-binding SDL_ClearError)

(define-function-binding SDL_GetError
  return: (c-string error-message))

(define-function-binding* SDL_SetError
  args: ((c-string msg))
  ;; We do not pass the string directly to SDL_SetError because it
  ;; could be a security vulnerability. We test for NULL to silence
  ;; annoying compiler warnings.
  body: "if (msg == NULL) { SDL_ClearError(); }
         else { SDL_SetError(\"%.1024s\", msg); }")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PLATFORM / HARDWARE

(define-function-binding SDL_GetPlatform
  return: (c-string platform-name))


#+libSDL-2.0.1+
(define-function-binding SDL_GetBasePath
  return: (c-string path))

#+libSDL-2.0.1+
(define-function-binding SDL_GetPrefPath
  return: (c-string path)
  args: ((c-string org)
         (c-string app)))


(define-function-binding SDL_GetPowerInfo
  return: (SDL_PowerState state)
  args: ((Sint32* secs_out)
         (Sint32* pct_out)))


#+libSDL-2.0.9+
(define-function-binding SDL_IsTablet
  return: (bool is_tablet))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CLIPBOARD TEXT

(define-function-binding SDL_HasClipboardText
  return: (bool has-text?))

(define-function-binding SDL_GetClipboardText
  ;; NOTE: c-string* (with asterisk) not c-string. The original string
  ;; will be automatically freed by CHICKEN after copying.
  return: (c-string* text))

(define-function-binding SDL_SetClipboardText
  return: (Sint32 zero-if-success)
  args: (((const c-string) text)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MESSAGE BOX

(define-function-binding SDL_ShowSimpleMessageBox
  return: (Sint32 zero-if-success)
  args: ((SDL_MessageBoxFlags flags)
         ((const c-string) title)
         ((const c-string) message)
         (SDL_Window*-or-null window)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VERSION

(define %current-version
  (foreign-lambda*
   void ((Uint8* majorOut) (Uint8* minorOut) (Uint8* patchOut))
   "SDL_version v;"
   "SDL_GetVersion(&v);"
   "*majorOut = v.major;"
   "*minorOut = v.minor;"
   "*patchOut = v.patch;"))

(define (current-version)
  (with-temp-mem ((major-out (%allocate-Uint8))
                  (minor-out (%allocate-Uint8))
                  (patch-out (%allocate-Uint8)))
    (%current-version major-out minor-out patch-out)
    (list (pointer-u8-ref major-out)
          (pointer-u8-ref minor-out)
          (pointer-u8-ref patch-out))))


(define %compiled-version
  (foreign-lambda*
   void ((Uint8* majorOut) (Uint8* minorOut) (Uint8* patchOut))
   "SDL_version v;"
   "SDL_VERSION(&v);"
   "*majorOut = v.major;"
   "*minorOut = v.minor;"
   "*patchOut = v.patch;"))

(define (compiled-version)
  (with-temp-mem ((major-out (%allocate-Uint8))
                  (minor-out (%allocate-Uint8))
                  (patch-out (%allocate-Uint8)))
    (%compiled-version major-out minor-out patch-out)
    (list (pointer-u8-ref major-out)
          (pointer-u8-ref minor-out)
          (pointer-u8-ref patch-out))))


(define-function-binding* SDL_VERSION_ATLEAST
  return: (bool at-least?)
  args: ((Sint32 x) (Sint32 y) (Sint32 z))
  body: "C_return( SDL_VERSION_ATLEAST(x, y, z) );")

(define (version-at-least? x y z)
  (SDL_VERSION_ATLEAST x y z))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MISC

#+libSDL-2.0.14+
(define-function-binding SDL_OpenURL
  return: (Sint32 zero-if-success)
  args: (((const c-string) url)))
