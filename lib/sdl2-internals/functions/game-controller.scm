;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export SDL_GameControllerOpen
        SDL_GameControllerClose
        SDL_GameControllerUpdate
        SDL_IsGameController

        SDL_GameControllerEventState
        SDL_GameControllerGetAttached
        SDL_GameControllerGetJoystick

        SDL_GameControllerMapping
        SDL_GameControllerMappingForGUID
        SDL_GameControllerAddMapping

        SDL_GameControllerName
        SDL_GameControllerNameForIndex

        SDL_GameControllerGetAxis
        SDL_GameControllerGetAxisFromString
        SDL_GameControllerGetStringForAxis

        SDL_GameControllerGetButton
        SDL_GameControllerGetButtonFromString
        SDL_GameControllerGetStringForButton

        SDL_GameControllerGetBindForAxis
        SDL_GameControllerGetBindForButton)

#+libSDL-2.0.2+
(export SDL_GameControllerAddMappingsFromFile
        SDL_GameControllerAddMappingsFromRW)

#+libSDL-2.0.4+
(export SDL_GameControllerFromInstanceID)

#+libSDL-2.0.6+
(export SDL_GameControllerGetVendor
        SDL_GameControllerGetProduct
        SDL_GameControllerGetProductVersion
        SDL_GameControllerNumMappings
        SDL_GameControllerMappingForIndex)

#+libSDL-2.0.9+
(export SDL_GameControllerGetPlayerIndex
        SDL_GameControllerMappingForDeviceIndex
        SDL_GameControllerRumble)


(define-function-binding SDL_GameControllerOpen
  return: (SDL_GameController* gamecontroller)
  args: ((Sint32 joystick_index)))

(define-function-binding SDL_GameControllerClose
  args: ((SDL_GameController* gamecontroller)))

#+libSDL-2.0.4+
(define-function-binding SDL_GameControllerFromInstanceID
  return: (SDL_GameController* gamecontroller)
  args: ((SDL_JoystickID joyid)))

(define-function-binding SDL_IsGameController
  return: (bool is_controller)
  args: ((Sint32 joystick_index)))


(define-function-binding SDL_GameControllerUpdate)

(define-function-binding SDL_GameControllerEventState
  return: (Sint32 result)
  args: ((Sint32 state)))

(define-function-binding SDL_GameControllerGetAttached
  return: (bool attached)
  args: ((SDL_GameController* gamecontroller)))

(define-function-binding SDL_GameControllerGetJoystick
  return: (SDL_Joystick* joystick)
  args: ((SDL_GameController* gamecontroller)))


(define-function-binding SDL_GameControllerMapping
  return: (c-string mapping)
  args: ((SDL_GameController* gamecontroller)))

(define-function-binding* SDL_GameControllerMappingForGUID
  return: (c-string mapping)
  args: ((SDL_JoystickGUID* guid))
  body: "C_return(SDL_GameControllerMappingForGUID(*guid));")

(define-function-binding SDL_GameControllerAddMapping
  return: (Sint32 result)
  args: (((const c-string) mappingString)))

#+libSDL-2.0.2+
(define-function-binding SDL_GameControllerAddMappingsFromFile
  return: (Sint32 result)
  args: (((const c-string) filename)))

#+libSDL-2.0.2+
(define-function-binding SDL_GameControllerAddMappingsFromRW
  return: (Sint32 result)
  args: ((SDL_RWops* rw)
         (bool freerw)))


(define-function-binding SDL_GameControllerName
  return: ((const c-string) name)
  args: ((SDL_GameController* gamecontroller)))

(define-function-binding SDL_GameControllerNameForIndex
  return: ((const c-string) name)
  args: ((int joystick_index)))


(define-function-binding SDL_GameControllerGetAxis
  return: (Sint16 value)
  args: ((SDL_GameController* gamecontroller)
         (SDL_GameControllerAxis axis)))

(define-function-binding SDL_GameControllerGetAxisFromString
  return: (SDL_GameControllerAxis axis)
  args: (((const c-string) pchString)))

(define-function-binding SDL_GameControllerGetStringForAxis
  return: (c-string str)
  args: ((SDL_GameControllerAxis axis)))


(define-function-binding SDL_GameControllerGetButton
  return: (bool value)
  args: ((SDL_GameController* gamecontroller)
         (SDL_GameControllerButton button)))

(define-function-binding SDL_GameControllerGetButtonFromString
  return: (SDL_GameControllerButton button)
  args: (((const c-string) pchString)))

(define-function-binding SDL_GameControllerGetStringForButton
  return: (c-string str)
  args: ((SDL_GameControllerButton button)))


;;; These are tricky, because they return SDL_GameControllerButtonBind
;;; structs by value, not pointers. CHICKEN FFI can't handle structs
;;; by value, so we must use hacks.

(define (SDL_GameControllerGetBindForAxis gamecontroller axis)
  (define foreign-getter
    (foreign-lambda*
     void ((SDL_GameController* gamecontroller)
           (SDL_GameControllerAxis axis)
           (SDL_GameControllerButtonBind* bind_out))
     "*bind_out = SDL_GameControllerGetBindForAxis(gamecontroller, axis);"))
  (let ((bind_out (%alloc-game-controller-button-bind)))
    (foreign-getter gamecontroller axis bind_out)
    bind_out))

(define (SDL_GameControllerGetBindForButton gamecontroller button)
  (define foreign-getter
    (foreign-lambda*
     void ((SDL_GameController* gamecontroller)
           (SDL_GameControllerButton button)
           (SDL_GameControllerButtonBind* bind_out))
     "*bind_out = SDL_GameControllerGetBindForButton(gamecontroller, button);"))
  (let ((bind_out (%alloc-game-controller-button-bind)))
    (foreign-getter gamecontroller button bind_out)
    bind_out))


#+libSDL-2.0.6+
(begin
  (define-function-binding SDL_GameControllerGetVendor
    return: (Uint16 vendor-id)
    args: ((SDL_GameController* gamecontroller)))

  (define-function-binding SDL_GameControllerGetProduct
    return: (Uint16 product-id)
    args: ((SDL_GameController* gamecontroller)))

  (define-function-binding SDL_GameControllerGetProductVersion
    return: (Uint16 product-version)
    args: ((SDL_GameController* gamecontroller)))


  (define-function-binding SDL_GameControllerNumMappings
    return: (Sint32 num))

  (define-function-binding SDL_GameControllerMappingForIndex
    return: (c-string mapping)
    args: ((Sint32 mapping_index))))


#+libSDL-2.0.9+
(begin
  (define-function-binding SDL_GameControllerGetPlayerIndex
    return: (Sint32 player)
    args: ((SDL_GameController* game_controller)))

  (define-function-binding SDL_GameControllerMappingForDeviceIndex
    return: (c-string mapping)
    args: ((Sint32 joystick_index)))

  (define-function-binding SDL_GameControllerRumble
    return: (Sint32 zero-on-success)
    args: ((SDL_GameController* gamecontroller)
           (Uint16 low_frequency_rumble)
           (Uint16 high_frequency_rumble)
           (Uint32 duration_ms))))
