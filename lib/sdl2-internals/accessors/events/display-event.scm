;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export display-event?
        display-event-display
        display-event-display-set!
        display-event-event
        display-event-event-set!
        display-event-event-raw
        display-event-event-raw-set!
        display-event-data1
        display-event-data1-set!
        display-event-orientation
        display-event-orientation-set!)


;;; NOTE: Public type declarations are in shared/reexport-types.scm

(cond-expand
 (libSDL-2.0.9+
  (define-event-type "SDL_DisplayEvent"
    types: (SDL_DISPLAYEVENT)
    pred:  display-event?
    print: ((display display-event-display)
            (event display-event-event)
            (data1 display-event-data1))
    ("display.display"
     type:   Uint32
     getter: display-event-display
     setter: display-event-display-set!
     guard:  (Uint32-guard "sdl2:display-event field display"))
    ("display.event"
     type:   Uint8
     getter: display-event-event-raw
     setter: display-event-event-raw-set!
     guard:  (Uint8-guard "sdl2:display-event field event"))
    ("display.data1"
     type:   Sint32
     getter: display-event-data1
     setter: display-event-data1-set!
     guard:  (Sint32-guard "sdl2:display-event field data1")))

  (define-enum-accessor
    getter: (display-event-event
             raw:  display-event-event-raw
             conv: display-event-id->symbol)
    setter: (display-event-event-set!
             raw:  display-event-event-raw-set!
             conv: symbol->display-event-id))

  (define-enum-accessor
    getter: (%display-event-orientation
             raw:  display-event-data1
             conv: display-orientation->symbol)
    setter: (display-event-orientation-set!
             raw:  display-event-data1-set!
             conv: symbol->display-orientation))

  (define (display-event-orientation event)
    (if (and (display-event? event)
             (= SDL_DISPLAYEVENT_ORIENTATION
                (display-event-event-raw event)))
        (%display-event-orientation event)
        (error 'display-event-orientation
               "not a display-event with event orientation" event)))

  (set! (setter display-event-orientation)
        display-event-orientation-set!))

 (else
  (define-versioned (display-event?) libSDL-2.0.9+ (void))
  (define-versioned (display-event-display) libSDL-2.0.9+ (void))
  (define-versioned (display-event-display-set!) libSDL-2.0.9+ (void))
  (define-versioned (display-event-event) libSDL-2.0.9+ (void))
  (define-versioned (display-event-event-set!) libSDL-2.0.9+ (void))
  (define-versioned (display-event-event-raw) libSDL-2.0.9+ (void))
  (define-versioned (display-event-event-raw-set!) libSDL-2.0.9+ (void))
  (define-versioned (display-event-data1) libSDL-2.0.9+ (void))
  (define-versioned (display-event-data1-set!) libSDL-2.0.9+ (void))
  (define-versioned (display-event-orientation) libSDL-2.0.9+ (void))
  (define-versioned (display-event-orientation-set!) libSDL-2.0.9+ (void))))
