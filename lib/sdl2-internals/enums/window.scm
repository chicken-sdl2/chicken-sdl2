;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOW POS

(define-foreign-constants Uint32
  SDL_WINDOWPOS_UNDEFINED
  SDL_WINDOWPOS_CENTERED
  SDL_WINDOWPOS_UNDEFINED_MASK
  SDL_WINDOWPOS_CENTERED_MASK)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOW FLAGS

(export symbol->window-flag
        window-flag->symbol
        pack-window-flags
        unpack-window-flags)


(define-enum-mappings
  type: SDL_WindowFlags
  symbol->value: symbol->window-flag
  value->symbol: window-flag->symbol

  ((required: libSDL-2.0.0+ "SDL_VERSION_ATLEAST(2,0,0)")
   (fullscreen          SDL_WINDOW_FULLSCREEN)
   (fullscreen-desktop  SDL_WINDOW_FULLSCREEN_DESKTOP)
   (opengl              SDL_WINDOW_OPENGL)
   (shown               SDL_WINDOW_SHOWN)
   (hidden              SDL_WINDOW_HIDDEN)
   (borderless          SDL_WINDOW_BORDERLESS)
   (resizable           SDL_WINDOW_RESIZABLE)
   (minimized           SDL_WINDOW_MINIMIZED)
   (maximized           SDL_WINDOW_MAXIMIZED)
   (input-grabbed       SDL_WINDOW_INPUT_GRABBED)
   (input-focus         SDL_WINDOW_INPUT_FOCUS)
   (mouse-focus         SDL_WINDOW_MOUSE_FOCUS)
   (foreign             SDL_WINDOW_FOREIGN))

  ((required: libSDL-2.0.1+ "SDL_VERSION_ATLEAST(2,0,1)")
   (allow-high-dpi      SDL_WINDOW_ALLOW_HIGHDPI))

  ((required: libSDL-2.0.4+ "SDL_VERSION_ATLEAST(2,0,4)")
   (mouse-capture       SDL_WINDOW_MOUSE_CAPTURE))

  ((required: libSDL-2.0.5+ "SDL_VERSION_ATLEAST(2,0,5)")
   (always-on-top       SDL_WINDOW_ALWAYS_ON_TOP))

  ((required: libSDL-2.0.6+ "SDL_VERSION_ATLEAST(2,0,6)")
   (vulkan              SDL_WINDOW_VULKAN)))


(define-enum-mask-packer pack-window-flags
  symbol->window-flag)


(define-enum-mask-unpacker unpack-window-flags
  window-flag->symbol
  (append
   (list SDL_WINDOW_FULLSCREEN
         SDL_WINDOW_FULLSCREEN_DESKTOP
         SDL_WINDOW_OPENGL
         SDL_WINDOW_SHOWN
         SDL_WINDOW_HIDDEN
         SDL_WINDOW_BORDERLESS
         SDL_WINDOW_RESIZABLE
         SDL_WINDOW_MINIMIZED
         SDL_WINDOW_MAXIMIZED
         SDL_WINDOW_INPUT_GRABBED
         SDL_WINDOW_INPUT_FOCUS
         SDL_WINDOW_MOUSE_FOCUS
         SDL_WINDOW_FOREIGN)
   (cond-expand
     (libSDL-2.0.1+ (list SDL_WINDOW_ALLOW_HIGHDPI))
     (else '()))
   (cond-expand
     (libSDL-2.0.4+ (list SDL_WINDOW_MOUSE_CAPTURE))
     (else '()))
   (cond-expand
     (libSDL-2.0.8+ (list SDL_WINDOW_VULKAN))
     (else '()))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOW EVENT IDS

(export symbol->window-event-id
        window-event-id->symbol)


(define-enum-mappings
  type: SDL_WindowEventID
  symbol->value: symbol->window-event-id
  value->symbol: window-event-id->symbol

  ((required: libSDL-2.0.0+ "SDL_VERSION_ATLEAST(2,0,0)")
   (none          SDL_WINDOWEVENT_NONE)
   (shown         SDL_WINDOWEVENT_SHOWN)
   (hidden        SDL_WINDOWEVENT_HIDDEN)
   (exposed       SDL_WINDOWEVENT_EXPOSED)
   (moved         SDL_WINDOWEVENT_MOVED)
   (resized       SDL_WINDOWEVENT_RESIZED)
   (size-changed  SDL_WINDOWEVENT_SIZE_CHANGED)
   (minimized     SDL_WINDOWEVENT_MINIMIZED)
   (maximized     SDL_WINDOWEVENT_MAXIMIZED)
   (restored      SDL_WINDOWEVENT_RESTORED)
   (enter         SDL_WINDOWEVENT_ENTER)
   (leave         SDL_WINDOWEVENT_LEAVE)
   (focus-gained  SDL_WINDOWEVENT_FOCUS_GAINED)
   (focus-lost    SDL_WINDOWEVENT_FOCUS_LOST)
   (close         SDL_WINDOWEVENT_CLOSE))

  ((required: libSDL-2.0.5+ "SDL_VERSION_ATLEAST(2,0,5)")
   (take-focus    SDL_WINDOWEVENT_TAKE_FOCUS)
   (hit-test      SDL_WINDOWEVENT_HIT_TEST)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOW FLASH OPERATION

#+libSDL-2.0.16+
(export symbol->flash-operation
        flash-operation->symbol)


#+libSDL-2.0.16+
(define-enum-mappings
  type: SDL_FlashOperation
  symbol->value: symbol->flash-operation
  value->symbol: flash-operation->symbol
  ((cancel          SDL_FLASH_CANCEL)
   (briefly         SDL_FLASH_BRIEFLY)
   (until-focused   SDL_FLASH_UNTIL_FOCUSED)))
