;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(define-foreign-constants SDL_eventaction
  SDL_ADDEVENT
  SDL_PEEKEVENT
  SDL_GETEVENT)


(define-foreign-constants int
  SDL_TEXTEDITINGEVENT_TEXT_SIZE
  SDL_TEXTINPUTEVENT_TEXT_SIZE)


(export symbol->event-type
        event-type->symbol
        %event-type-exists?
        %add-event-type!)


(define (symbol->event-type symbol #!optional not-found-callback)
  ;; First check if it is a built-in event type.
  (or (%symbol->event-type symbol (lambda (_) #f))
      ;; Next check if it is a user event type.
      (alist-ref symbol %symbol->user-event-alist eq? #f)
      ;; Otherwise invoke the callback or signal an error.
      (if not-found-callback
          (not-found-callback symbol)
          (error "invalid enum symbol" symbol))))


(define (event-type->symbol value #!optional not-found-callback)
  ;; First check if it is a built-in event type.
  (or (%event-type->symbol value (lambda (_) #f))
      ;; Next check if it is a user event type.
      (alist-ref value %user-event->symbol-alist = #f)
      ;; Otherwise invoke the callback or signal an error.
      (if not-found-callback
          (not-found-callback value)
          (error "invalid enum value" value))))


;;; Mappings for the built-in event types.
(define-enum-mappings
  type: SDL_EventType
  symbol->value: %symbol->event-type
  value->symbol: %event-type->symbol

  ((required: libSDL-2.0.0+ "SDL_VERSION_ATLEAST(2,0,0)")
   (first                       SDL_FIRSTEVENT)
   (quit                        SDL_QUIT)
   (app-terminating             SDL_APP_TERMINATING)
   (app-low-memory              SDL_APP_LOWMEMORY)
   (app-will-enter-background   SDL_APP_WILLENTERBACKGROUND)
   (app-did-enter-background    SDL_APP_DIDENTERBACKGROUND)
   (app-will-enter-foreground   SDL_APP_WILLENTERFOREGROUND)
   (app-did-enter-foreground    SDL_APP_DIDENTERFOREGROUND)
   (window                      SDL_WINDOWEVENT)
   (sys-wm                      SDL_SYSWMEVENT)
   (key-down                    SDL_KEYDOWN)
   (key-up                      SDL_KEYUP)
   (text-editing                SDL_TEXTEDITING)
   (text-input                  SDL_TEXTINPUT)
   (mouse-motion                SDL_MOUSEMOTION)
   (mouse-button-down           SDL_MOUSEBUTTONDOWN)
   (mouse-button-up             SDL_MOUSEBUTTONUP)
   (mouse-wheel                 SDL_MOUSEWHEEL)
   (joy-axis-motion             SDL_JOYAXISMOTION)
   (joy-ball-motion             SDL_JOYBALLMOTION)
   (joy-hat-motion              SDL_JOYHATMOTION)
   (joy-button-down             SDL_JOYBUTTONDOWN)
   (joy-button-up               SDL_JOYBUTTONUP)
   (joy-device-added            SDL_JOYDEVICEADDED)
   (joy-device-removed          SDL_JOYDEVICEREMOVED)
   (controller-axis-motion      SDL_CONTROLLERAXISMOTION)
   (controller-button-down      SDL_CONTROLLERBUTTONDOWN)
   (controller-button-up        SDL_CONTROLLERBUTTONUP)
   (controller-device-added     SDL_CONTROLLERDEVICEADDED)
   (controller-device-removed   SDL_CONTROLLERDEVICEREMOVED)
   (controller-device-remapped  SDL_CONTROLLERDEVICEREMAPPED)
   (finger-down                 SDL_FINGERDOWN)
   (finger-up                   SDL_FINGERUP)
   (finger-motion               SDL_FINGERMOTION)
   (dollar-gesture              SDL_DOLLARGESTURE)
   (dollar-record               SDL_DOLLARRECORD)
   (multi-gesture               SDL_MULTIGESTURE)
   (clipboard-update            SDL_CLIPBOARDUPDATE)
   (drop-file                   SDL_DROPFILE)
   (last                        SDL_LASTEVENT))

  ((required: libSDL-2.0.5+ "SDL_VERSION_ATLEAST(2,0,5)")
   (drop-text                   SDL_DROPTEXT)
   (drop-begin                  SDL_DROPBEGIN)
   (drop-complete               SDL_DROPCOMPLETE))

  ((required: libSDL-2.0.9+ "SDL_VERSION_ATLEAST(2,0,9)")
   (display                     SDL_DISPLAYEVENT)))


;;; The first event type registered with register-events! will be
;;; assigned to SDL_USEREVENT.
(define-foreign-constants SDL_EventType
  SDL_USEREVENT)



;;; Association lists to hold user event types.
(define %user-event->symbol-alist '())
(define %symbol->user-event-alist '())


;;; Return true if the given symbol is either a built-in event type or
;;; user event type.
(: %event-type-exists? (symbol -> boolean))
(define (%event-type-exists? sym)
  (not (not (symbol->event-type sym (lambda (_) #f)))))


(: %add-event-type! (symbol integer -> void))
(define (%add-event-type! event-symbol event-number)
  (set! %user-event->symbol-alist
        (cons (cons event-number event-symbol)
              %user-event->symbol-alist))
  (set! %symbol->user-event-alist
        (cons (cons event-symbol event-number)
              %symbol->user-event-alist))
  (void))
