;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


#+libSDL-2.0.9+
(begin
  (export symbol->display-event-id
          display-event-id->symbol

          symbol->display-orientation
          display-orientation->symbol)


  (define-enum-mappings
    type: SDL_DisplayEventID
    symbol->value: symbol->display-event-id
    value->symbol: display-event-id->symbol

    ((required: libSDL-2.0.9+ "SDL_VERSION_ATLEAST(2,0,9)")
     (none          SDL_DISPLAYEVENT_NONE)
     (orientation   SDL_DISPLAYEVENT_ORIENTATION))
    ((required: libSDL-2.0.14+ "SDL_VERSION_ATLEAST(2,0,14)")
     (connected     SDL_DISPLAYEVENT_CONNECTED)
     (disconnected  SDL_DISPLAYEVENT_DISCONNECTED)))


  (define-enum-mappings
    type: SDL_DisplayOrientation
    symbol->value: symbol->display-orientation
    value->symbol: display-orientation->symbol

    ((unknown            SDL_ORIENTATION_UNKNOWN)
     (landscape          SDL_ORIENTATION_LANDSCAPE)
     (landscape-flipped  SDL_ORIENTATION_LANDSCAPE_FLIPPED)
     (portrait           SDL_ORIENTATION_PORTRAIT)
     (portrait-flipped   SDL_ORIENTATION_PORTRAIT_FLIPPED))))
