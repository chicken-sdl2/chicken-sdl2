;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export symbol->joystick-hat-position
        joystick-hat-position->symbol)

(define-enum-mappings
  type: SDL_JoystickHatPosition
  symbol->value: symbol->joystick-hat-position
  value->symbol: joystick-hat-position->symbol

  ((centered    SDL_HAT_CENTERED)
   (up          SDL_HAT_UP)
   (right       SDL_HAT_RIGHT)
   (down        SDL_HAT_DOWN)
   (left        SDL_HAT_LEFT)
   (right-up    SDL_HAT_RIGHTUP)
   (right-down  SDL_HAT_RIGHTDOWN)
   (left-up     SDL_HAT_LEFTUP)
   (left-down   SDL_HAT_LEFTDOWN)))


#+libSDL-2.0.4+
(begin
  (export symbol->joystick-power-level
          joystick-power-level->symbol)

  (define-enum-mappings
    type: SDL_JoystickPowerLevel
    symbol->value: symbol->joystick-power-level
    value->symbol: joystick-power-level->symbol

    ((unknown  SDL_JOYSTICK_POWER_UNKNOWN)
     (empty    SDL_JOYSTICK_POWER_EMPTY)
     (low      SDL_JOYSTICK_POWER_LOW)
     (medium   SDL_JOYSTICK_POWER_MEDIUM)
     (full     SDL_JOYSTICK_POWER_FULL)
     (wired    SDL_JOYSTICK_POWER_WIRED)
     (max      SDL_JOYSTICK_POWER_MAX))))


#+libSDL-2.0.6+
(begin
  (export symbol->joystick-type
          joystick-type->symbol)

  (define-enum-mappings
    type: SDL_JoystickType
    symbol->value: symbol->joystick-type
    value->symbol: joystick-type->symbol

    ((unknown          SDL_JOYSTICK_TYPE_UNKNOWN)
     (game-controller  SDL_JOYSTICK_TYPE_GAMECONTROLLER)
     (wheel            SDL_JOYSTICK_TYPE_WHEEL)
     (arcade-stick     SDL_JOYSTICK_TYPE_ARCADE_STICK)
     (flight-stick     SDL_JOYSTICK_TYPE_FLIGHT_STICK)
     (dance-pad        SDL_JOYSTICK_TYPE_DANCE_PAD)
     (guitar           SDL_JOYSTICK_TYPE_GUITAR)
     (drum-kit         SDL_JOYSTICK_TYPE_DRUM_KIT)
     (arcade-pad       SDL_JOYSTICK_TYPE_ARCADE_PAD)
     (throttle         SDL_JOYSTICK_TYPE_THROTTLE))))
