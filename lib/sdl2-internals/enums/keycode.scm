;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; KEYCODE

(export symbol->keycode
        keycode->symbol)


(define-enum-mappings
  type: SDL_Keycode
  symbol->value: symbol->keycode
  value->symbol: keycode->symbol

  ((required: libSDL-2.0.0+ "SDL_VERSION_ATLEAST(2,0,0)")
   (unknown              SDLK_UNKNOWN)
   (return               SDLK_RETURN)
   (escape               SDLK_ESCAPE)
   (backspace            SDLK_BACKSPACE)
   (tab                  SDLK_TAB)
   (space                SDLK_SPACE)
   (exclaim              SDLK_EXCLAIM)
   (quote-dbl            SDLK_QUOTEDBL)
   (hash                 SDLK_HASH)
   (percent              SDLK_PERCENT)
   (dollar               SDLK_DOLLAR)
   (ampersand            SDLK_AMPERSAND)
   (quote                SDLK_QUOTE)
   (left-paren           SDLK_LEFTPAREN)
   (right-paren          SDLK_RIGHTPAREN)
   (asterisk             SDLK_ASTERISK)
   (plus                 SDLK_PLUS)
   (comma                SDLK_COMMA)
   (minus                SDLK_MINUS)
   (period               SDLK_PERIOD)
   (slash                SDLK_SLASH)
   (n-0                  SDLK_0)
   (n-1                  SDLK_1)
   (n-2                  SDLK_2)
   (n-3                  SDLK_3)
   (n-4                  SDLK_4)
   (n-5                  SDLK_5)
   (n-6                  SDLK_6)
   (n-7                  SDLK_7)
   (n-8                  SDLK_8)
   (n-9                  SDLK_9)
   (colon                SDLK_COLON)
   (semicolon            SDLK_SEMICOLON)
   (less                 SDLK_LESS)
   (equals               SDLK_EQUALS)
   (greater              SDLK_GREATER)
   (question             SDLK_QUESTION)
   (at                   SDLK_AT)

   (left-bracket         SDLK_LEFTBRACKET)
   (backslash            SDLK_BACKSLASH)
   (right-bracket        SDLK_RIGHTBRACKET)
   (caret                SDLK_CARET)
   (underscore           SDLK_UNDERSCORE)
   (backquote            SDLK_BACKQUOTE)

   (a                    SDLK_a)
   (b                    SDLK_b)
   (c                    SDLK_c)
   (d                    SDLK_d)
   (e                    SDLK_e)
   (f                    SDLK_f)
   (g                    SDLK_g)
   (h                    SDLK_h)
   (i                    SDLK_i)
   (j                    SDLK_j)
   (k                    SDLK_k)
   (l                    SDLK_l)
   (m                    SDLK_m)
   (n                    SDLK_n)
   (o                    SDLK_o)
   (p                    SDLK_p)
   (q                    SDLK_q)
   (r                    SDLK_r)
   (s                    SDLK_s)
   (t                    SDLK_t)
   (u                    SDLK_u)
   (v                    SDLK_v)
   (w                    SDLK_w)
   (x                    SDLK_x)
   (y                    SDLK_y)
   (z                    SDLK_z)

   (caps-lock            SDLK_CAPSLOCK)

   (f1                   SDLK_F1)
   (f2                   SDLK_F2)
   (f3                   SDLK_F3)
   (f4                   SDLK_F4)
   (f5                   SDLK_F5)
   (f6                   SDLK_F6)
   (f7                   SDLK_F7)
   (f8                   SDLK_F8)
   (f9                   SDLK_F9)
   (f10                  SDLK_F10)
   (f11                  SDLK_F11)
   (f12                  SDLK_F12)

   (print-screen         SDLK_PRINTSCREEN)
   (scroll-lock          SDLK_SCROLLLOCK)
   (pause                SDLK_PAUSE)
   (insert               SDLK_INSERT)
   (home                 SDLK_HOME)
   (page-up              SDLK_PAGEUP)
   (delete               SDLK_DELETE)
   (end                  SDLK_END)
   (page-down            SDLK_PAGEDOWN)
   (right                SDLK_RIGHT)
   (left                 SDLK_LEFT)
   (down                 SDLK_DOWN)
   (up                   SDLK_UP)

   (num-lock-clear       SDLK_NUMLOCKCLEAR)
   (kp-divide            SDLK_KP_DIVIDE)
   (kp-multiply          SDLK_KP_MULTIPLY)
   (kp-minus             SDLK_KP_MINUS)
   (kp-plus              SDLK_KP_PLUS)
   (kp-enter             SDLK_KP_ENTER)
   (kp-1                 SDLK_KP_1)
   (kp-2                 SDLK_KP_2)
   (kp-3                 SDLK_KP_3)
   (kp-4                 SDLK_KP_4)
   (kp-5                 SDLK_KP_5)
   (kp-6                 SDLK_KP_6)
   (kp-7                 SDLK_KP_7)
   (kp-8                 SDLK_KP_8)
   (kp-9                 SDLK_KP_9)
   (kp-0                 SDLK_KP_0)
   (kp-period            SDLK_KP_PERIOD)

   (application          SDLK_APPLICATION)
   (power                SDLK_POWER)
   (kp-equals            SDLK_KP_EQUALS)
   (f13                  SDLK_F13)
   (f14                  SDLK_F14)
   (f15                  SDLK_F15)
   (f16                  SDLK_F16)
   (f17                  SDLK_F17)
   (f18                  SDLK_F18)
   (f19                  SDLK_F19)
   (f20                  SDLK_F20)
   (f21                  SDLK_F21)
   (f22                  SDLK_F22)
   (f23                  SDLK_F23)
   (f24                  SDLK_F24)
   (execute              SDLK_EXECUTE)
   (help                 SDLK_HELP)
   (menu                 SDLK_MENU)
   (select               SDLK_SELECT)
   (stop                 SDLK_STOP)
   (again                SDLK_AGAIN)
   (undo                 SDLK_UNDO)
   (cut                  SDLK_CUT)
   (copy                 SDLK_COPY)
   (paste                SDLK_PASTE)
   (find                 SDLK_FIND)
   (mute                 SDLK_MUTE)
   (volume-up            SDLK_VOLUMEUP)
   (volume-down          SDLK_VOLUMEDOWN)
   (kp-comma             SDLK_KP_COMMA)
   (kp-equals-as400      SDLK_KP_EQUALSAS400)

   (alt-erase            SDLK_ALTERASE)
   (sys-req              SDLK_SYSREQ)
   (cancel               SDLK_CANCEL)
   (clear                SDLK_CLEAR)
   (prior                SDLK_PRIOR)
   (return2              SDLK_RETURN2)
   (separator            SDLK_SEPARATOR)
   (out                  SDLK_OUT)
   (oper                 SDLK_OPER)
   (clear-again          SDLK_CLEARAGAIN)
   (crsel                SDLK_CRSEL)
   (exsel                SDLK_EXSEL)

   (kp-00                SDLK_KP_00)
   (kp-000               SDLK_KP_000)
   (thousands-separator  SDLK_THOUSANDSSEPARATOR)
   (decimal-separator    SDLK_DECIMALSEPARATOR)
   (currency-unit        SDLK_CURRENCYUNIT)
   (currency-subunit     SDLK_CURRENCYSUBUNIT)
   (kp-left-paren        SDLK_KP_LEFTPAREN)
   (kp-right-paren       SDLK_KP_RIGHTPAREN)
   (kp-left-brace        SDLK_KP_LEFTBRACE)
   (kp-right-brace       SDLK_KP_RIGHTBRACE)
   (kp-tab               SDLK_KP_TAB)
   (kp-backspace         SDLK_KP_BACKSPACE)
   (kp-a                 SDLK_KP_A)
   (kp-b                 SDLK_KP_B)
   (kp-c                 SDLK_KP_C)
   (kp-d                 SDLK_KP_D)
   (kp-e                 SDLK_KP_E)
   (kp-f                 SDLK_KP_F)
   (kp-xor               SDLK_KP_XOR)
   (kp-power             SDLK_KP_POWER)
   (kp-percent           SDLK_KP_PERCENT)
   (kp-less              SDLK_KP_LESS)
   (kp-greater           SDLK_KP_GREATER)
   (kp-ampersand         SDLK_KP_AMPERSAND)
   (kp-dbl-ampersand     SDLK_KP_DBLAMPERSAND)
   (kp-vertical-bar      SDLK_KP_VERTICALBAR)
   (kp-dbl-vertical-bar  SDLK_KP_DBLVERTICALBAR)
   (kp-colon             SDLK_KP_COLON)
   (kp-hash              SDLK_KP_HASH)
   (kp-space             SDLK_KP_SPACE)
   (kp-at                SDLK_KP_AT)
   (kp-exclam            SDLK_KP_EXCLAM)
   (kp-mem-store         SDLK_KP_MEMSTORE)
   (kp-mem-recall        SDLK_KP_MEMRECALL)
   (kp-mem-clear         SDLK_KP_MEMCLEAR)
   (kp-mem-add           SDLK_KP_MEMADD)
   (kp-mem-subtract      SDLK_KP_MEMSUBTRACT)
   (kp-mem-multiply      SDLK_KP_MEMMULTIPLY)
   (kp-mem-divide        SDLK_KP_MEMDIVIDE)
   (kp-plus-minus        SDLK_KP_PLUSMINUS)
   (kp-clear             SDLK_KP_CLEAR)
   (kp-clear-entry       SDLK_KP_CLEARENTRY)
   (kp-binary            SDLK_KP_BINARY)
   (kp-octal             SDLK_KP_OCTAL)
   (kp-decimal           SDLK_KP_DECIMAL)
   (kp-hexadecimal       SDLK_KP_HEXADECIMAL)

   (lctrl                SDLK_LCTRL)
   (lshift               SDLK_LSHIFT)
   (lalt                 SDLK_LALT)
   (lgui                 SDLK_LGUI)
   (rctrl                SDLK_RCTRL)
   (rshift               SDLK_RSHIFT)
   (ralt                 SDLK_RALT)
   (rgui                 SDLK_RGUI)

   (mode                 SDLK_MODE)

   (audio-next           SDLK_AUDIONEXT)
   (audio-prev           SDLK_AUDIOPREV)
   (audio-stop           SDLK_AUDIOSTOP)
   (audio-play           SDLK_AUDIOPLAY)
   (audio-mute           SDLK_AUDIOMUTE)
   (media-select         SDLK_MEDIASELECT)
   (www                  SDLK_WWW)
   (mail                 SDLK_MAIL)
   (calculator           SDLK_CALCULATOR)
   (computer             SDLK_COMPUTER)
   (ac-search            SDLK_AC_SEARCH)
   (ac-home              SDLK_AC_HOME)
   (ac-back              SDLK_AC_BACK)
   (ac-forward           SDLK_AC_FORWARD)
   (ac-stop              SDLK_AC_STOP)
   (ac-refresh           SDLK_AC_REFRESH)
   (ac-bookmarks         SDLK_AC_BOOKMARKS)

   (brightness-down      SDLK_BRIGHTNESSDOWN)
   (brightness-up        SDLK_BRIGHTNESSUP)
   (display-switch       SDLK_DISPLAYSWITCH)
   (kbd-illum-toggle     SDLK_KBDILLUMTOGGLE)
   (kbd-illum-down       SDLK_KBDILLUMDOWN)
   (kbd-illum-up         SDLK_KBDILLUMUP)
   (eject                SDLK_EJECT)
   (sleep                SDLK_SLEEP))

  ((required: libSDL-2.0.6+ "SDL_VERSION_ATLEAST(2,0,6)")
   (app1                 SDLK_APP1)
   (app2                 SDLK_APP2)
   (audio-rewind         SDLK_AUDIOREWIND)
   (audio-fast-forward   SDLK_AUDIOFASTFORWARD)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; KEYMOD

(export symbol->keymod
        keymod->symbol
        pack-keymods
        unpack-keymods)


(define-enum-mappings
  type: SDL_Keymod
  symbol->value: symbol->keymod
  value->symbol: keymod->symbol

  ((none    KMOD_NONE)

   (lshift  KMOD_LSHIFT)
   (rshift  KMOD_RSHIFT)
   (shift   KMOD_SHIFT)

   (lctrl   KMOD_LCTRL)
   (rctrl   KMOD_RCTRL)
   (ctrl    KMOD_CTRL)

   (lalt    KMOD_LALT)
   (ralt    KMOD_RALT)
   (alt     KMOD_ALT)

   (lgui    KMOD_LGUI)
   (rgui    KMOD_RGUI)
   (gui     KMOD_GUI)

   (num     KMOD_NUM)
   (caps    KMOD_CAPS)
   (mode    KMOD_MODE)))


(define-enum-mask-packer pack-keymods
  symbol->keymod)

(define-enum-mask-unpacker unpack-keymods
  keymod->symbol
  (list
   ;; omitted: KMOD_NONE
   KMOD_SHIFT  KMOD_LSHIFT  KMOD_RSHIFT
   KMOD_CTRL   KMOD_LCTRL   KMOD_RCTRL
   KMOD_ALT    KMOD_LALT    KMOD_RALT
   KMOD_GUI    KMOD_LGUI    KMOD_RGUI
   KMOD_NUM
   KMOD_CAPS
   KMOD_MODE))
