# chicken-sdl2

[CHICKEN Scheme](http://call-cc.org/) bindings to
[Simple DirectMedia Layer](http://libsdl.org/) 2 (SDL2).

- Version:     0.4.1 (2021-10-21)
- Project:     https://gitlab.com/chicken-sdl2/chicken-sdl2
- Issues:      https://gitlab.com/chicken-sdl2/chicken-sdl2/issues
- Docs:        http://wiki.call-cc.org/eggref/5/sdl2
- License:     [BSD 2-Clause](LICENSE-BSD.txt)
- Maintainer:  John Croisant (john+chicken at croisant dot net)


## Synopsis

chicken-sdl2 (aka "the sdl2 egg") makes it easy to use
Simple DirectMedia Layer version 2 (SDL2) from the CHICKEN Scheme language.
SDL2 is a cross-platform library for making games and other software.

chicken-sdl2 provides a programmer-friendly, convenient, and
CHICKEN-idiomatic interface to SDL2. It takes care of the annoying
low-level C stuff for you, so you can focus on making your game.

If a feature you need is not yet available in chicken-sdl2, please
[file a feature request](CONTRIBUTING.md#filing-feature-requests) or
contact a maintainer, so we can prioritize adding it.


## Installation

If you run into trouble installing chicken-sdl2, please
[file a support request](CONTRIBUTING.md#filing-support-requests) so
we can help you, and so we can improve the install process and
instructions for future users.

### Dependencies

chicken-sdl2 requires **SDL version 2.0.0 or higher**.
It will not work with SDL 1. (For SDL 1, try the
[sdl-base](http://wiki.call-cc.org/eggref/4/sdl-base) egg instead.)

chicken-sdl2 requires CHICKEN 4.8 or higher.
As of version 0.3.0, it is compatible with both CHICKEN 4 and CHICKEN 5.

### Installing the egg

In most cases, you can install the sdl2 egg by running this command:

```
chicken-install sdl2
```

The installer will try to automatically determine the SDL2 compiler and linker flags
using the `sdl2-config` command.

In special cases, you may need to set the `SDL2_CFLAGS` and `SDL2_LDFLAGS`
environment variables to provide the compiler and linker flags (respectively),
then try again. For example:

```
export SDL2_CFLAGS="-I/usr/local/include/SDL2"
export SDL2_LDFLAGS="-L/usr/local/lib -lSDL2"
chicken-install sdl2
```

These environment variables are needed only when installing the egg itself.
They are not needed when compiling or running games that use the egg.

### Installing on macOS

On macOS, the sdl2 egg can be compiled using either UNIX-style libraries
(e.g. SDL2 compiled from source or installed via Homebrew)
or Mac-style frameworks (e.g. downloaded from the SDL website).

When automatically determining compiler and linker flags on macOS,
the installer will first check to see if `sdl2-config` is available.
If it is available, the installer will use it to determine the flags.

If `sdl2-config` is not available,
the installer will check to see if `SDL2.framework` is available
in either the `/Library/Frameworks` or `/System/Library/Frameworks` directories.
If so, the installer will use the framework.

If the framework is installed in some other directory,
or if you want to force using the framework even when `sdl2-config` is available,
set the `SDL2_CFLAGS` and `SDL2_LDFLAGS` enviroment variables
to tell the compiler where to find the framework:

```
export SDL2_CFLAGS="-F/path/to/your/frameworks"
export SDL2_LDFLAGS="-F/path/to/your/frameworks -framework SDL2"
chicken-install sdl2
```

### Installing from git

If you want to try out the cutting edge, not-yet-released version of this library,
you can clone the git repository and install, like so:

```
git clone https://gitlab.com/chicken-sdl2/chicken-sdl2.git
cd chicken-sdl2
chicken-install
```


## Demos and Examples

After you have installed chicken-sdl2, you can try compiling and
running chicken-sdl2 demos and examples.

The [demos directory](https://gitlab.com/chicken-sdl2/chicken-sdl2/tree/master/demos)
contains small programs demonstrating how to use various features of chicken-sdl2.
For example, to compile and run "The Sea and The Stars" demo:

```
csc -O3 ./demos/sea-and-stars.scm
./demos/sea-and-stars
```

The [chicken-sdl2-examples repository](https://gitlab.com/chicken-sdl2/chicken-sdl2-examples)
contains complete example games and programs made with chicken-sdl2 and related libraries.


## Related libraries

- [chicken-sdl2-image](https://gitlab.com/chicken-sdl2/chicken-sdl2-image):
  Bindings to [SDL_image 2](http://www.libsdl.org/projects/SDL_image/)
  (image file loading)

- [chicken-sdl2-ttf](https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf):
  Bindings to [SDL_ttf 2](http://www.libsdl.org/projects/SDL_ttf/)
  (text rendering using TTF fonts)


## Contributing

chicken-sdl2 is a volunteer effort, and your help is appreciated.
There are many ways to get involved in the project, whether you are an
experienced programmer or not. For more information about how you can
help, please see the [contribution guide](CONTRIBUTING.md).

Please be aware that all project participants are expected to abide by
the [Contributor Code of Conduct](CODE_OF_CONDUCT.md). We are
committed to making participation in this project a welcoming and
harassment-free experience.
